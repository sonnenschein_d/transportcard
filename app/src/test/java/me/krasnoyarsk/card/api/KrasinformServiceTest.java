package me.krasnoyarsk.card.api;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.concurrent.TimeUnit;

import io.reactivex.observers.TestObserver;
import me.krasnoyarsk.card.api.objects.CardDetails;
import me.krasnoyarsk.card.api.objects.HistoryDetails;
import me.krasnoyarsk.card.test.util.Data;
import okhttp3.OkHttpClient;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import okhttp3.mockwebserver.RecordedRequest;
import pl.droidsonroids.retrofit2.JspoonConverterFactory;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class KrasinformServiceTest {

    private static final String PATH_CARD_DETAILS = "/card/transport/";
    private static final String HEADER_REFERER = "Referer";
    private static final String METHOD_POST = "POST";
    private static final String PATH_HISTORY_DETAILS = "/card/transport/0100000005948737/04.2018/detail/";
    private static final String METHOD_GET = "GET";
    private static final String ENCODED_PATH_HISTORY_DETAILS;

    static {
        String encoded;
        try {
            encoded = "/" + URLEncoder.encode(PATH_HISTORY_DETAILS, "utf-8");
        } catch (UnsupportedEncodingException e) {
            encoded = PATH_HISTORY_DETAILS;
        }
        ENCODED_PATH_HISTORY_DETAILS = encoded;
    }

    private MockWebServer mockServer;
    private KrasinformService service;

    @Before
    public void setUp() throws IOException {
        mockServer = new MockWebServer();
        mockServer.start();

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(1, TimeUnit.SECONDS) // For testing purposes
                .readTimeout(1, TimeUnit.SECONDS) // For testing purposes
                .writeTimeout(1, TimeUnit.SECONDS)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(mockServer.url("/"))
                .addConverterFactory(JspoonConverterFactory.create())
                .client(okHttpClient)
                .build();

        service = retrofit.create(KrasinformService.class);
    }

    @After
    public void tearDown() throws IOException {
        mockServer.shutdown();
    }

    @Test
    public void getSuccessfullyCardDetails() throws InterruptedException {
        TestObserver<CardDetails> testObserver = new TestObserver<CardDetails>() {
            @Override
            public void onNext(CardDetails cardDetails) {
                super.onNext(cardDetails);

                int expectedBalance = 445;
                int actualBalance = cardDetails.getBalance().getState();
                assertEquals(expectedBalance, actualBalance);
            }
        };

        //mock response
        MockResponse mockResponse = new MockResponse()
                .setResponseCode(200)
                .setBody(Data.CARD_DETAILS_HTML);
        mockServer.enqueue(mockResponse);

        //call the api
        service.getCardDetails(Data.CARD_NUMBER, Data.CARD_TYPE).subscribe(testObserver);
        testObserver.awaitTerminalEvent(2, TimeUnit.SECONDS);

        //no errors
        testObserver.assertNoErrors();
        testObserver.assertValueCount(1);

        RecordedRequest request = mockServer.takeRequest(2, TimeUnit.SECONDS);
        assertEquals(PATH_CARD_DETAILS, request.getPath());
        assertNotNull(request.getHeader(HEADER_REFERER));
        assertEquals(METHOD_POST, request.getMethod());
    }

    @Test
    public void getCardDetailsError() throws InterruptedException{
        TestObserver<CardDetails> testObserver = new TestObserver<>();
        MockResponse mockResponse = new MockResponse()
                .setResponseCode(500); // Simulate a 500 HTTP Code
        mockServer.enqueue(mockResponse);

        service.getCardDetails(Data.CARD_NUMBER, Data.CARD_TYPE).subscribe(testObserver);
        testObserver.awaitTerminalEvent(2, TimeUnit.SECONDS);

        testObserver.assertNoValues();
        assertEquals(1, testObserver.errorCount());

        RecordedRequest request = mockServer.takeRequest(2, TimeUnit.SECONDS);
        assertEquals(PATH_CARD_DETAILS, request.getPath());
        assertNotNull(request.getHeader(HEADER_REFERER));
        assertEquals(METHOD_POST, request.getMethod());
    }

    @Test
    public void getCardDetails_SocketTimeoutException() throws InterruptedException{
        TestObserver<CardDetails> testObserver = new TestObserver<>();
        MockResponse mockResponse = new MockResponse()
                .setResponseCode(200)
                .throttleBody(256, 2, TimeUnit.SECONDS) // Simulate SocketTimeout
                .setBody(Data.CARD_DETAILS_HTML);
        mockServer.enqueue(mockResponse);

        service.getCardDetails(Data.CARD_NUMBER, Data.CARD_TYPE).subscribe(testObserver);
        testObserver.awaitTerminalEvent(2, TimeUnit.SECONDS);

        testObserver.assertNoValues();
        assertEquals(1, testObserver.errorCount());

        RecordedRequest request = mockServer.takeRequest(2, TimeUnit.SECONDS);
        assertEquals(PATH_CARD_DETAILS, request.getPath());
        assertNotNull(request.getHeader(HEADER_REFERER));
        assertEquals(METHOD_POST, request.getMethod());
    }

    @Test
    public void getHistoryDetailsSuccessfully() throws InterruptedException, UnsupportedEncodingException{
        TestObserver<HistoryDetails> testObserver = new TestObserver<>();

        //mock response
        MockResponse mockResponse = new MockResponse()
                .setResponseCode(200)
                .setBody(Data.HISTORY_DETAILS_HTML);
        mockServer.enqueue(mockResponse);

        //call the api
        service.getHistoryDetails(PATH_HISTORY_DETAILS).subscribe(testObserver);
        testObserver.awaitTerminalEvent(2, TimeUnit.SECONDS);

        //no errors
        testObserver.assertNoErrors();
        testObserver.assertValueCount(1);

        RecordedRequest request = mockServer.takeRequest(2, TimeUnit.SECONDS);
        assertEquals(ENCODED_PATH_HISTORY_DETAILS, request.getPath());
        assertEquals(METHOD_GET, request.getMethod());
    }
}
