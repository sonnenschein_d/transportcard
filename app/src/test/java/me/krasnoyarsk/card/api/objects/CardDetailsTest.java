package me.krasnoyarsk.card.api.objects;

import org.junit.Test;

import java.text.ParseException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import me.krasnoyarsk.card.api.objects.CardDetails;
import me.krasnoyarsk.card.test.util.Data;
import me.krasnoyarsk.card.util.DateUtil;
import pl.droidsonroids.jspoon.HtmlAdapter;
import pl.droidsonroids.jspoon.Jspoon;

import static me.krasnoyarsk.card.api.util.ApiConstants.REGEX_NUMBER;
import static org.junit.Assert.assertEquals;

public class CardDetailsTest {

    private static final String CARD_NUMBER = "0100000005948737";

    private static final String TYPE = "Электронный проездной";
    private static final String STATE = "445";
    private static final int STATE_INT = 445;
    private static final String VALID_UNTIL = "";

    private static final String PAYMENT_DATE = "20.03.2018 18:56:16";
    private static final String PAYMENT_NUMBER = "9412261 DB3BFE7275970993";
    private static final int PAYMENT_VALUE = 1000;
    private static final String PAYMENT_TOP_UP = "Эл. кошелек 1000 т.е.";
    private static final String PAYMENT_RECEIVER = "ТС Красноярск 80";

    private static final String RIDES_PERIOD = "04.2018";
    private static final String RIDES_TYPE = "Эл. кошелек";
    private static final int RIDES_QUANTITY = 16;
    private static final int RIDES_CHARGED = 336;
    private static final String RIDES_DETAILS_LINK = "/card/transport/0100000005948737/04.2018/detail/";

    @Test
    public void testPattern(){
        String pattern = REGEX_NUMBER;
        String input = "445 т.е.";
        String expected = "445";
        String actual = parseInput(pattern, input);
        assertEquals(expected, actual);
    }

    private String parseInput(String regex, String input) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(input);
        if (matcher.find()) {
            return (matcher.groupCount() > 0) ? matcher.group(1) : "";
        }
        return "";
    }

    @Test
    public void parseCardBalanceResponseHtml() throws ParseException{
        Jspoon jspoon = Jspoon.create();
        HtmlAdapter<CardDetails> htmlAdapter = jspoon.adapter(CardDetails.class);

        CardDetails details = htmlAdapter.fromHtml(Data.CARD_DETAILS_HTML);

        assertEquals(CARD_NUMBER, details.getCardNumber());

        assertEquals(TYPE, details.getBalance().getType());
        assertEquals(STATE_INT, details.getBalance().getState());
        assertEquals(VALID_UNTIL, details.getBalance().getValidUntil());

        assertEquals(4, details.getPayments().size());
        assertEquals(6, details.getHistory().size());

        assertEquals(DateUtil.fromString("dd.MM.yyyy HH:mm:ss", PAYMENT_DATE), details.getPayments().get(0).getDateTime());
        assertEquals(PAYMENT_NUMBER, details.getPayments().get(0).getNumber());
        assertEquals(PAYMENT_VALUE, details.getPayments().get(0).getValue());
        assertEquals(PAYMENT_TOP_UP, details.getPayments().get(0).getTopUp());
        assertEquals(PAYMENT_RECEIVER, details.getPayments().get(0).getReceiver());

        assertEquals(DateUtil.fromString("MM.yyyy", RIDES_PERIOD), details.getHistory().get(0).getPeriod());
        assertEquals(RIDES_TYPE, details.getHistory().get(0).getType());
        assertEquals(RIDES_QUANTITY, details.getHistory().get(0).getQuantity());
        assertEquals(RIDES_CHARGED, details.getHistory().get(0).getCharged());
        assertEquals(RIDES_DETAILS_LINK, details.getHistory().get(0).getDetailsLink());
    }
}
