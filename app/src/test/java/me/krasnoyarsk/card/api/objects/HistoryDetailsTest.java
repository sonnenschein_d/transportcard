package me.krasnoyarsk.card.api.objects;

import org.junit.Test;

import java.text.ParseException;

import me.krasnoyarsk.card.api.objects.HistoryDetails;
import me.krasnoyarsk.card.test.util.Data;
import me.krasnoyarsk.card.util.DateUtil;
import pl.droidsonroids.jspoon.HtmlAdapter;
import pl.droidsonroids.jspoon.Jspoon;

import static org.junit.Assert.assertEquals;

public class HistoryDetailsTest {

    private static final String CARD_NUMBER = "0100000005948737";

    private static final String RIDE_DATE = "12.04.2018 18:30:24";
    private static final String RIDE_TYPE = "Эл. кошелек";
    private static final String RIDE_ROUTE = "63";
    private static final int RIDE_CHARGED = 21;

    @Test
    public void parseHistoryDetailsResponseHtml() throws ParseException{
        Jspoon jspoon = Jspoon.create();
        HtmlAdapter<HistoryDetails> htmlAdapter = jspoon.adapter(HistoryDetails.class);
        HistoryDetails details = htmlAdapter.fromHtml(Data.HISTORY_DETAILS_HTML);

        assertEquals(CARD_NUMBER, details.getCardNumber());

        assertEquals(2, details.getRides().size());

        assertEquals(DateUtil.fromString("dd.MM.yyyy HH:mm:ss", RIDE_DATE), details.getRides().get(0).getDateTime());
        assertEquals(RIDE_TYPE, details.getRides().get(0).getType());
        assertEquals(RIDE_CHARGED, details.getRides().get(0).getCharged());
        assertEquals(RIDE_ROUTE, details.getRides().get(0).getRoute());
    }
}
