package me.krasnoyarsk.card.test.util;

public final class Data {

    public static final String CARD_NUMBER = "0100000005948737";
    public static final String CARD_TYPE = "transport";

    public static final String CARD_DETAILS_HTML = "<div class=\"main-col\">\n" +
            "                        <div class=\"page-header\">\n" +
            "                            <h2>Информация по транспортной карте</h2>\n" +
            "                        </div>\n" +
            "                        <p>\n" +
            "                            <strong>Номер транспортной карты:</strong> 0100000005948737\n" +
            "                        </p>\n" +
            "                        <hr class=\"divider\">\n" +
            "                        <div class=\"page-header\">\n" +
            "                            <h3>Текущий баланс **</h3>\n" +
            "                        </div>\n" +
            "                        <table class=\"table\">\n" +
            "                            <tr>\n" +
            "                                <th>Тип проездного</th>\n" +
            "                                <th>Состояние</th>\n" +
            "                                <th>Период действия</th>\n" +
            "                            </tr>\n" +
            "                            <tr>\n" +
            "                                <td>Электронный проездной</td>\n" +
            "                                <td>445 т.е.</td>\n" +
            "                                <td></td>\n" +
            "                            </tr>\n" +
            "                        </table>\n" +
            "                        <hr class=\"divider\">\n" +
            "                        <div class=\"page-header\">\n" +
            "                            <h3>Платежи *</h3>\n" +
            "                        </div>\n" +
            "                        <table class=\"table\">\n" +
            "                            <tr>\n" +
            "                                <th>Дата и время пополнения</th>\n" +
            "                                <th>Номер платежа\n" +
            "                                    <br>Номер чека\n" +
            "                                </th>\n" +
            "                                <th>Стоимость пополнения</th>\n" +
            "                                <th>Зачислено</th>\n" +
            "                                <th>Приемщик</th>\n" +
            "                            </tr>\n" +
            "                            <tr>\n" +
            "                                <td>20.03.2018 18:56:16</td>\n" +
            "                                <td>9412261\n" +
            "                                    <br>DB3BFE7275970993\n" +
            "                                </td>\n" +
            "                                <td>1000</td>\n" +
            "                                <td>Эл. кошелек\n" +
            "                                    <br>1000 т.е.\n" +
            "                                    <br>\n" +
            "                                </td>\n" +
            "                                <td>ТС Красноярск 80</td>\n" +
            "                            </tr>\n" +
            "                            <tr>\n" +
            "                                <td>05.02.2018 19:08:17</td>\n" +
            "                                <td>9230953\n" +
            "                                    <br>58D1B1DAB7368883\n" +
            "                                </td>\n" +
            "                                <td>1000</td>\n" +
            "                                <td>Эл. кошелек\n" +
            "                                    <br>1000 т.е.\n" +
            "                                    <br>\n" +
            "                                </td>\n" +
            "                                <td>ТС Красноярск 80</td>\n" +
            "                            </tr>\n" +
            "                            <tr>\n" +
            "                                <td>19.12.2017 19:06:35</td>\n" +
            "                                <td>9058388\n" +
            "                                    <br>7F35CD90380EC4A8\n" +
            "                                </td>\n" +
            "                                <td>1000</td>\n" +
            "                                <td>Эл. кошелек\n" +
            "                                    <br>1000 т.е.\n" +
            "                                    <br>\n" +
            "                                </td>\n" +
            "                                <td>ТС Красноярск 80</td>\n" +
            "                            </tr>\n" +
            "                            <tr>\n" +
            "                                <td>11.11.2017 15:42:43</td>\n" +
            "                                <td>8892971\n" +
            "                                    <br>DED0EE684966056F\n" +
            "                                </td>\n" +
            "                                <td>1000</td>\n" +
            "                                <td>Эл. кошелек\n" +
            "                                    <br>1000 т.е.\n" +
            "                                    <br>\n" +
            "                                </td>\n" +
            "                                <td>ТС Красноярск 80</td>\n" +
            "                            </tr>\n" +
            "                        </table>\n" +
            "                        <hr class=\"divider\">\n" +
            "                        <div class=\"page-header\">\n" +
            "                            <h3>Поездки *</h3>\n" +
            "                        </div>\n" +
            "                        <table class=\"table\">\n" +
            "                            <tr>\n" +
            "                                <th>Период</th>\n" +
            "                                <th>Тип проездного</th>\n" +
            "                                <th>Количество</th>\n" +
            "                                <th>Списано</th>\n" +
            "                                <th>Детализация</th>\n" +
            "                            </tr>\n" +
            "                            <tr>\n" +
            "                                <td rowspan=\"1\">04.2018</td>\n" +
            "                                <td>Эл. кошелек</td>\n" +
            "                                <td>16</td>\n" +
            "                                <td>336</td>\n" +
            "                                <td rowspan=\"1\">\n" +
            "                                    <a href=\"/card/transport/0100000005948737/04.2018/detail/\">Детализация</a>\n" +
            "                                </td>\n" +
            "                            </tr>\n" +
            "                            <tr>\n" +
            "                                <td rowspan=\"1\">03.2018</td>\n" +
            "                                <td>Эл. кошелек</td>\n" +
            "                                <td>31</td>\n" +
            "                                <td>651</td>\n" +
            "                                <td rowspan=\"1\">\n" +
            "                                    <a href=\"/card/transport/0100000005948737/03.2018/detail/\">Детализация</a>\n" +
            "                                </td>\n" +
            "                            </tr>\n" +
            "                            <tr>\n" +
            "                                <td rowspan=\"1\">02.2018</td>\n" +
            "                                <td>Эл. кошелек</td>\n" +
            "                                <td>39</td>\n" +
            "                                <td>811</td>\n" +
            "                                <td rowspan=\"1\">\n" +
            "                                    <a href=\"/card/transport/0100000005948737/02.2018/detail/\">Детализация</a>\n" +
            "                                </td>\n" +
            "                            </tr>\n" +
            "                            <tr>\n" +
            "                                <td rowspan=\"1\">01.2018</td>\n" +
            "                                <td>Эл. кошелек</td>\n" +
            "                                <td>23</td>\n" +
            "                                <td>476</td>\n" +
            "                                <td rowspan=\"1\">\n" +
            "                                    <a href=\"/card/transport/0100000005948737/01.2018/detail/\">Детализация</a>\n" +
            "                                </td>\n" +
            "                            </tr>\n" +
            "                            <tr>\n" +
            "                                <td rowspan=\"1\">12.2017</td>\n" +
            "                                <td>Эл. кошелек</td>\n" +
            "                                <td>43</td>\n" +
            "                                <td>898</td>\n" +
            "                                <td rowspan=\"1\">\n" +
            "                                    <a href=\"/card/transport/0100000005948737/12.2017/detail/\">Детализация</a>\n" +
            "                                </td>\n" +
            "                            </tr>\n" +
            "                            <tr>\n" +
            "                                <td rowspan=\"1\">11.2017</td>\n" +
            "                                <td>Эл. кошелек</td>\n" +
            "                                <td>39</td>\n" +
            "                                <td>815</td>\n" +
            "                                <td rowspan=\"1\">\n" +
            "                                    <a href=\"/card/transport/0100000005948737/11.2017/detail/\">Детализация</a>\n" +
            "                                </td>\n" +
            "                            </tr>\n" +
            "                        </tr>\n" +
            "                    </table>\n" +
            "                    <hr class=\"divider\">\n" +
            "                    <p class=\"help-block\">\n" +
            "                * — за период с 01.11.2017 по 01.05.2018.\n" +
            "            </p>\n" +
            "                    <p class=\"help-block\">\n" +
            "                ** — по последним данным, поступившим от перевозчиков. Возможна задержка поступления данных о совершенных поездках.\n" +
            "            </p>\n" +
            "                </div>";


    public static final String HISTORY_DETAILS_HTML = "<div class=\"main-col\">\n" +
                    "                        <div class=\"page-header\">\n" +
                    "                            <h2>Информация по транспортной карте</h2>\n" +
                    "                        </div>\n" +
                    "                        <p>\n" +
                    "                            <strong>Номер транспортной карты:</strong> 0100000005948737\n" +
                    "                        </p>\n" +
                    "                        <hr class=\"divider\">\n" +
                    "                        <div class=\"page-header\">\n" +
                    "                            <h3>Поездки за 04.2018 (всего: 18) *</h3>\n" +
                    "                        </div>\n" +
                    "                        <table class=\"table\">\n" +
                    "                            <tr>\n" +
                    "                                <th>Дата поездки</th>\n" +
                    "                                <th>Тип поездки</th>\n" +
                    "                                <th>Номер маршрута</th>\n" +
                    "                                <th>Списано</th>\n" +
                    "                            </tr>\n" +
                    "                            <tr>\n" +
                    "                                <td>12.04.2018 18:30:24</td>\n" +
                    "                                <td>Эл. кошелек</td>\n" +
                    "                                <td>63</td>\n" +
                    "                                <td>21</td>\n" +
                    "                            </tr>\n" +
                    "                            <tr>\n" +
                    "                                <td>12.04.2018 08:12:35</td>\n" +
                    "                                <td>Эл. кошелек</td>\n" +
                    "                                <td></td>\n" +
                    "                                <td>21</td>\n" +
                    "                            </tr>\n" +
                    "                        </table>\n" +
                    "                        <hr class=\"divider\">\n" +
                    "                        <div class=\"page-header\">\n" +
                    "                            <h3>Итого:</h3>\n" +
                    "                        </div>\n" +
                    "                        <table class=\"table\">\n" +
                    "                            <tr>\n" +
                    "                                <th>Тип поездки</th>\n" +
                    "                                <th>Количество</th>\n" +
                    "                                <th>Списано</th>\n" +
                    "                            </tr>\n" +
                    "                            <tr>\n" +
                    "                                <td>Эл. кошелек</td>\n" +
                    "                                <td>18</td>\n" +
                    "                                <td>378</td>\n" +
                    "                            </tr>\n" +
                    "                        </table>\n" +
                    "                        <hr class=\"divider\">\n" +
                    "                        <p class=\"help-block\">\n" +
                    "        * - по последним данным, поступивших от перевозчиков. Возможна задержка поступления данных о совершенных поездках.\n" +
                    "                            <br/>\n" +
                    "                        </p>\n" +
                    "                        <p class=\"help-block\">\n" +
                    "        ** - поездка возвращена.\n" +
                    "                            <br/>\n" +
                    "                        </p>\n" +
                    "                        <p class=\"help-block\">\n" +
                    "        — - маршрут не определен, не поступила информация о маршруте от перевозчика.\n" +
                    "    </p>\n" +
                    "                        <a class=\"pull-right\" href=\"javascript:history.back();\">Вернуться назад</a>\n" +
                    "                        <div class='clearfix'></div>\n" +
                    "                        <p class=\"help-block\">\n" +
                    "        Страница сформирована 13.04.2018 10:26:28\n" +
                    "    </p>\n" +
                    "                    </div>";

    private Data(){throw new IllegalAccessError();}


}
