package me.krasnoyarsk.card.repository.resource;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.net.SocketTimeoutException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.FlowableEmitter;
import io.reactivex.Observable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.TestScheduler;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.doCallRealMethod;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@SuppressWarnings("unchecked")
@RunWith(MockitoJUnitRunner.class)
public class NetworkBoundListResourceTest {

    @Mock
    public NetworkBoundListResource<List<Integer>, List<Integer>> resource;
    @Mock
    public FlowableEmitter<Resource<List<Integer>>> emitter;

    private TestScheduler scheduler;

    private int remoteCalled;

    @Before
    public void init(){
        scheduler = new TestScheduler();
        resource.scheduler = scheduler;
        remoteCalled = 0;
    }

    @Test
    public void errorOccurred_whenFetchingLocalData(){
        doCallRealMethod().when(resource).fetch(emitter);

        final SQLException exception = new SQLException("Some problem with db");
        doReturn(Flowable.fromCallable(() -> {
            throw exception;
        })).when(resource).getLocal();

        resource.fetch(emitter);
        scheduler.triggerActions();

        verify(resource, never()).fetchRemote(any());
        verify(resource, never()).maybeStale(anyList());
        verify(emitter, never()).onNext(any());
        verify(emitter, times(1)).onError(exception);
    }

    @Test
    public void fetchRemote_everythingOk() throws Exception{
        List<Integer> remoteData = Arrays.asList(1,2,3);
        doCallRealMethod().when(resource).fetchRemote(emitter);

        doReturn(Observable.just(remoteData)).when(resource).getRemote();
        Function<List<Integer>, List<Integer>> mapperFunc = Mockito.mock(Function.class);
        doReturn(mapperFunc).when(resource).mapper();
        doNothing().when(resource).saveCallResult(anyList());
        doReturn(remoteData).when(mapperFunc).apply(remoteData);

        resource.fetchRemote(emitter);
        scheduler.triggerActions();

        verify(resource, times(1)).getRemote();
        verify(mapperFunc, times(1)).apply(remoteData);
        verify(resource,times(1)).saveCallResult(remoteData);
        verify(emitter, never()).onNext(any());
    }

    @Test
    public void fetchRemote_isRetriedOnError() throws Exception{
        List<Integer> remoteData = Arrays.asList(1,2,3);
        doCallRealMethod().when(resource).fetchRemote(emitter);

        doReturn(Observable.fromCallable(() -> {
            if (remoteCalled < 2){
                remoteCalled += 1;
                throw new SocketTimeoutException("Imitating slow network");
            } else {
                return remoteData;
            }
        })).when(resource).getRemote();
        Function<List<Integer>, List<Integer>> mapperFunc = Mockito.mock(Function.class);
        doReturn(mapperFunc).when(resource).mapper();
        doNothing().when(resource).saveCallResult(anyList());
        doReturn(remoteData).when(mapperFunc).apply(remoteData);

        resource.fetchRemote(emitter);
        scheduler.triggerActions();

        verify(resource, times(1)).getRemote();
        verify(mapperFunc, times(1)).apply(remoteData);
        verify(resource,times(1)).saveCallResult(remoteData);
        verify(emitter, never()).onNext(any());
    }

    @Test
    public void fetchRemote_errorEmitted_ifRetryDidNotHelp() throws Exception{
        doCallRealMethod().when(resource).fetchRemote(emitter);

        String msg = "Imitating slow network";
        doReturn(Observable.fromCallable(() -> {
            throw new SocketTimeoutException(msg);
        })).when(resource).getRemote();
        Function<List<Integer>, List<Integer>> mapperFunc = Mockito.mock(Function.class);
        doReturn(mapperFunc).when(resource).mapper();

        resource.fetchRemote(emitter);
        scheduler.triggerActions();

        verify(resource, times(1)).getRemote();
        verify(mapperFunc, never()).apply(anyList());
        verify(resource, never()).saveCallResult(anyList());
        verify(emitter, times(1)).onNext(Resource.error(msg, null));
    }

    @Test
    public void localData_isFresh(){
        List<Integer> localData = Arrays.asList(1,2,3);
        doCallRealMethod().when(resource).fetch(emitter);

        doReturn(false).when(resource).maybeStale(anyList());
        doReturn(Flowable.just(localData)).when(resource).getLocal();

        resource.fetch(emitter);
        scheduler.triggerActions();

        verify(resource, never()).fetchRemote(any());
        verify(resource, times(1)).maybeStale(anyList());
        verify(emitter, never()).onError(any());
        verify(emitter, times(1)).onNext(Resource.success(localData));
    }

    @Test
    public void noLocalData_fetchRemote_shouldBeCalled(){
        List<Integer> localData = Collections.emptyList();
        doCallRealMethod().when(resource).fetch(emitter);

        doReturn(Flowable.just(localData)).when(resource).getLocal();

        resource.fetch(emitter);
        scheduler.triggerActions();

        verify(resource, times(1)).fetchRemote(emitter);
        verify(emitter, never()).onError(any());
        verify(emitter, times(1)).onNext(Resource.loading(localData));
    }

    @Test
    public void staleLocalData_fetchRemote_shouldBeCalled(){
        List<Integer> localData = Arrays.asList(1,2,3);
        doCallRealMethod().when(resource).fetch(emitter);

        doReturn(true).when(resource).maybeStale(anyList());
        doReturn(Flowable.just(localData)).when(resource).getLocal();

        resource.fetch(emitter);
        scheduler.triggerActions();

        verify(resource, times(1)).fetchRemote(emitter);
        verify(emitter, never()).onError(any());
        verify(emitter, times(1)).onNext(Resource.loading(localData));
    }
}
