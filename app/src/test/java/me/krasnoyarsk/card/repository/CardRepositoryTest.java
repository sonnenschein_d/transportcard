package me.krasnoyarsk.card.repository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.Executor;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import me.krasnoyarsk.card.api.KrasinformService;
import me.krasnoyarsk.card.db.dao.CardDao;
import me.krasnoyarsk.card.db.entities.Card;
import me.krasnoyarsk.card.db.util.CardExtensions;
import me.krasnoyarsk.card.repository.CardRepository.FetchTaskCreator;
import me.krasnoyarsk.card.repository.CardRepository.TaskCreator;
import me.krasnoyarsk.card.repository.CardRepository.UpdateTaskCreator;
import me.krasnoyarsk.card.repository.resource.Resource;
import me.krasnoyarsk.card.repository.task.Task;
import me.krasnoyarsk.card.test.util.AssertRx;
import me.krasnoyarsk.card.util.AppExecutors;

import static me.krasnoyarsk.card.test.util.TestData.card;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class CardRepositoryTest {

    @Mock
    KrasinformService service;
    @Mock
    CardDao dao;
    @Mock
    AppExecutors executors;
    @Mock
    CardExtensions extensions;

    private Card card;
    private CardRepository repository;

    @Before
    public void setUp(){
        repository = create();
        card = card();
    }

    private CardRepository create(){
        return spy(new CardRepository(service, dao, executors, extensions));
    }

    @Test
    public void cards(){

        List<Card> cards = Collections.singletonList(card());
        doReturn(Flowable.just(cards)).when(dao).getAll();

        Flowable<Resource<List<Card>>> res = repository.cards();
        AssertRx.singleResult(res, Resource.success(cards));
    }

    @Test
    public void fetchWithNumberAndType(){
        doReturn(card).when(extensions).cardFor(anyString(), anyString());
        doReturn(Observable.just(false)).when(repository).fetch(any(Card.class));

        repository.fetch("0000000000000000", "transport");

        verify(repository, times(1)).fetch(card);
    }

    @Test
    public void doTask(){
        TaskCreator creator = mock(TaskCreator.class);
        Task task = mock(Task.class);

        Executor executor = mock(Executor.class);

        doReturn(task).when(creator).create(any(Card.class));
        doReturn(executor).when(executors).getNetworkIO();
        doReturn(Observable.just(false)).when(task).isLoading();

        repository.doTask(creator, card);

        verify(executor, times(1)).execute(task);
    }

    @Test
    public void fetchWithCard(){
        FetchTaskCreator creator = mock(FetchTaskCreator.class);
        repository.fetchTaskCreator = creator;

        doReturn(Observable.just(false)).when(repository).doTask(any(), any());

        repository.fetch(card);

        verify(repository, times(1)).doTask(creator, card);
    }

    @Test
    public void updateWithCard(){
        UpdateTaskCreator creator = mock(UpdateTaskCreator.class);
        repository.updateTaskCreator = creator;

        doReturn(Observable.just(false)).when(repository).doTask(any(), any());

        repository.update(card);

        verify(repository, times(1)).doTask(creator, card);
    }
}
