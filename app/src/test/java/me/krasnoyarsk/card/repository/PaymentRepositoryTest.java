package me.krasnoyarsk.card.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.text.ParseException;
import java.util.Collections;
import java.util.List;

import io.reactivex.Flowable;
import me.krasnoyarsk.card.db.dao.PaymentDao;
import me.krasnoyarsk.card.db.entities.Payment;
import me.krasnoyarsk.card.repository.resource.Resource;
import me.krasnoyarsk.card.test.util.AssertRx;

import static me.krasnoyarsk.card.test.util.TestData.payment;
import static org.mockito.Mockito.doReturn;

@RunWith(MockitoJUnitRunner.class)
public class PaymentRepositoryTest {

    public static final long NUMBER = 1L;

    @Mock
    PaymentDao dao;

    @Test
    public void paymentsByCard() throws ParseException {
        PaymentRepository repository = new PaymentRepository(dao);

        List<Payment> payments = Collections.singletonList(payment());
        doReturn(Flowable.just(payments)).when(dao).getByCard(NUMBER);

        Flowable<Resource<List<Payment>>> res = repository.paymentsByCard(NUMBER);
        AssertRx.singleResult(res, Resource.success(payments));
    }
}
