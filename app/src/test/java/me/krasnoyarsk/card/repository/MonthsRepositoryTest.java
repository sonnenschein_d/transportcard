package me.krasnoyarsk.card.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.text.ParseException;
import java.util.Collections;
import java.util.List;

import io.reactivex.Flowable;
import me.krasnoyarsk.card.db.dao.MonthStatDao;
import me.krasnoyarsk.card.db.entities.MonthsStat;
import me.krasnoyarsk.card.repository.resource.Resource;
import me.krasnoyarsk.card.test.util.AssertRx;

import static me.krasnoyarsk.card.test.util.TestData.stat;
import static org.mockito.Mockito.doReturn;

@RunWith(MockitoJUnitRunner.class)
public class MonthsRepositoryTest {

    public static final long NUMBER = 1L;
    @Mock
    MonthStatDao dao;

    @Test
    public void monthsByCard() throws ParseException {
        MonthsRepository repository = new MonthsRepository(dao);

        List<MonthsStat> stats = Collections.singletonList(stat());
        doReturn(Flowable.just(stats)).when(dao).getByCard(NUMBER);

        Flowable<Resource<List<MonthsStat>>> res = repository.monthsByCard(NUMBER);
        AssertRx.singleResult(res, Resource.success(stats));
    }
}
