package me.krasnoyarsk.card.repository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.text.ParseException;
import java.util.Collections;
import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.FlowableEmitter;
import io.reactivex.Observable;
import me.krasnoyarsk.card.api.KrasinformService;
import me.krasnoyarsk.card.api.objects.HistoryDetails;
import me.krasnoyarsk.card.db.CardDb;
import me.krasnoyarsk.card.db.dao.MonthStatDao;
import me.krasnoyarsk.card.db.dao.RideDao;
import me.krasnoyarsk.card.db.entities.MonthsStat;
import me.krasnoyarsk.card.db.entities.Ride;
import me.krasnoyarsk.card.repository.RideRepository.RidesResource;
import me.krasnoyarsk.card.repository.resource.Resource;
import me.krasnoyarsk.card.test.util.AssertRx;

import static me.krasnoyarsk.card.test.util.TestData.ride;
import static me.krasnoyarsk.card.test.util.TestData.stat;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class RideRepositoryTest {

    @Mock
    FlowableEmitter<Resource<List<Ride>>> emitter;
    @Mock
    KrasinformService service;
    @Mock
    CardDb db;
    @Mock
    RideDao rideDao;
    @Mock
    MonthStatDao monthDao;

    private MonthsStat stat;
    private RidesResource ridesResource;

    @Before
    public void setUp() throws ParseException {
        stat = stat();
        doReturn(rideDao).when(db).rideDao();
        doReturn(monthDao).when(db).monthDao();
        doReturn(Flowable.just(Collections.singletonList(ride()))).when(rideDao).getByCardAndMonth(anyLong(), any());
        ridesResource = spy(new RidesResource(emitter, stat, service, db));
    }

    @Test
    public void mapper() throws Exception {
        HistoryDetails details = mock(HistoryDetails.class);
        List<Ride> rides = Collections.singletonList(ride());
        doReturn(rides).when(details).getRides();

        List<Ride> result = ridesResource.mapper().apply(details);
        assertEquals(rides, result);
    }

    @Test
    public void getLocal() throws ParseException {
        AssertRx.singleResultList(ridesResource.getLocal(), ride());
    }

    @Test
    public void getRemote() throws ParseException{
        HistoryDetails details = mock(HistoryDetails.class);
        doReturn(Observable.just(details)).when(service).getHistoryDetails(any());
        AssertRx.singleResult(ridesResource.getRemote(), details);
    }

    @Test
    public void saveCallResult() throws ParseException {
        List<Ride> rides = Collections.singletonList(ride());
        ridesResource.saveCallResult(rides);
        verify(db).runInTransaction(any(Runnable.class));
    }

}
