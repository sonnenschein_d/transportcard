package me.krasnoyarsk.card.api.interceptor;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import me.krasnoyarsk.card.api.util.NetworkState;
import okhttp3.CacheControl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

import static me.krasnoyarsk.card.api.util.ApiConstants.HEADER_CACHE_CONTROL;
import static me.krasnoyarsk.card.api.util.ApiConstants.HEADER_EXPIRES;
import static me.krasnoyarsk.card.api.util.ApiConstants.HEADER_PRAGMA;

public class OfflineCacheInterceptor implements Interceptor {

    private NetworkState state;

    public OfflineCacheInterceptor(NetworkState state) {
        this.state = state;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();

        if (!state.isConnected()) {
            CacheControl cacheControl = new CacheControl.Builder()
                    .maxStale(7, TimeUnit.DAYS)
                    .build();

            request = request.newBuilder()
                    .removeHeader(HEADER_EXPIRES)
                    .removeHeader(HEADER_PRAGMA)
                    .removeHeader(HEADER_CACHE_CONTROL)
                    .cacheControl(cacheControl)
                    .build();
        }

        return chain.proceed(request);
    }
}
