package me.krasnoyarsk.card.api.converter;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import me.krasnoyarsk.card.api.objects.CardDetails;
import me.krasnoyarsk.card.api.objects.HistoryDetails;
import okhttp3.ResponseBody;
import pl.droidsonroids.jspoon.Jspoon;
import pl.droidsonroids.jspoon.exception.EmptySelectorException;
import retrofit2.Converter;
import retrofit2.Retrofit;

public class MyJspoonConverterFactory extends Converter.Factory {
    private Jspoon jspoon;

    public static MyJspoonConverterFactory create() {
        return new MyJspoonConverterFactory(Jspoon.create());
    }

    public static MyJspoonConverterFactory create(Jspoon jspoon) {
        return new MyJspoonConverterFactory(jspoon);
    }

    private MyJspoonConverterFactory(Jspoon jspoon) {
        this.jspoon = jspoon;
    }

    @Override
    public Converter<ResponseBody, ?> responseBodyConverter(Type type, Annotation[] annotations, Retrofit retrofit) {

        try {
            if (type == CardDetails.class){
                return new CardDetailsConverter(retrofit.baseUrl(), jspoon.adapter(CardDetails.class));
            } else if (type == HistoryDetails.class){
                return new HistoryDetailsConverter(retrofit.baseUrl(), jspoon.adapter(HistoryDetails.class));
            } else {
                return new DefaultConverter<>(retrofit.baseUrl(),
                        jspoon.adapter((Class<?>) type));
            }
        } catch (EmptySelectorException ex) {
            return null; // Let retrofit choose another converter
        }
    }
}
