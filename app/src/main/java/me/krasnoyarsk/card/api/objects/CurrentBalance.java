package me.krasnoyarsk.card.api.objects;

import pl.droidsonroids.jspoon.annotation.Selector;

import static me.krasnoyarsk.card.api.util.ApiConstants.REGEX_NUMBER;

/*
Детали текущего баланса

                        <table class="table">
                            <tr>
                                <th>Тип проездного</th>
                                <th>Состояние</th>
                                <th>Период действия</th>
                            </tr>
                            <tr>
                                <td>Электронный проездной</td>
                                <td>445 т.е.</td>
                                <td></td>
                            </tr>
                        </table>
 */
public class CurrentBalance {

    /*
    Тип проездного
     */
    @Selector("td:first-child")
    private String type;

    /*
    Состояние
     */
    @Selector(value = "td:nth-child(2)", regex = REGEX_NUMBER)
    private int state;

    /*
    Период действия
    maybe empty
     */
    @Selector("td:nth-child(3)")
    private String validUntil;

    private CurrentBalance(){}

    public CurrentBalance(String type, int state, String validUntil) {
        this.type = type;
        this.state = state;
        this.validUntil = validUntil;
    }

    public String getType() {
        return type;
    }

    public int getState() {
        return state;
    }

    public String getValidUntil() {
        return validUntil;
    }

    @Override
    public String toString() {
        return "CurrentBalance{" +
                "type='" + type + '\'' +
                ", state='" + state + '\'' +
                ", validUntil='" + validUntil + '\'' +
                '}';
    }
}
