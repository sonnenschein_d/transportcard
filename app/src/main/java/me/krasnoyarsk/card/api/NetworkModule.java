package me.krasnoyarsk.card.api;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import me.krasnoyarsk.card.api.converter.MyJspoonConverterFactory;
import me.krasnoyarsk.card.api.interceptor.CacheInterceptor;
import me.krasnoyarsk.card.api.interceptor.OfflineCacheInterceptor;
import me.krasnoyarsk.card.api.util.NetworkState;
import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import okhttp3.logging.HttpLoggingInterceptor.Level;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import timber.log.Timber;

import static me.krasnoyarsk.card.api.util.ApiConstants.BASE_URL;
import static me.krasnoyarsk.card.api.util.ApiConstants.CACHE_SIZE;

@Module
public class NetworkModule {

    @Singleton
    @Provides
    KrasinformService provideKrasinformService(Retrofit retrofit) {
        return retrofit.create(KrasinformService.class);
    }

    @Singleton
    @Provides
    Retrofit provideRetrofit(OkHttpClient client){
        return new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(MyJspoonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }

    @Singleton
    @Provides
    OkHttpClient provideClient(HttpLoggingInterceptor logging,
                               Cache cache,
                               CacheInterceptor cacheInterceptor,
                               OfflineCacheInterceptor offlineCacheInterceptor){
        return new OkHttpClient.Builder()
                .cache(cache)
                .addInterceptor(logging)
                .addInterceptor(offlineCacheInterceptor)
                .addNetworkInterceptor(cacheInterceptor)
                //TODO: add cookies interceptors if necessary
                .build();
    }

    @Singleton
    @Provides
    Cache provideCache(Context context){
        Cache cache = null;
        try {
            cache = new Cache(context.getCacheDir(), CACHE_SIZE);
        } catch (Exception e) {
            Timber.e(e);
        }
        return cache;
    }

    @Singleton
    @Provides
    HttpLoggingInterceptor provideLoggingInterceptor(){
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(Level.HEADERS);
        return logging;
    }

    @Singleton
    @Provides
    CacheInterceptor provideCacheInterceptor(NetworkState state){
        return new CacheInterceptor(state);
    }

    @Singleton
    @Provides
    OfflineCacheInterceptor provideOfflineCacheInterceptor(NetworkState state){
        return new OfflineCacheInterceptor(state);
    }

    @Singleton
    @Provides
    NetworkState provideNetworkState(Context context){
        return new NetworkState(context);
    }

}
