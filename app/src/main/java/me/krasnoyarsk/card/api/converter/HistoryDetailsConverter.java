package me.krasnoyarsk.card.api.converter;

import java.io.IOException;

import me.krasnoyarsk.card.api.objects.HistoryDetails;
import me.krasnoyarsk.card.db.entities.Ride;
import okhttp3.HttpUrl;
import okhttp3.ResponseBody;
import pl.droidsonroids.jspoon.HtmlAdapter;

import static me.krasnoyarsk.card.util.DateUtil.getMonth;

public class HistoryDetailsConverter extends AbstractConverter<HistoryDetails>{

    public static final String FIELD_MONTH = "month";
    public static final String FIELD_CARD_NUMBER = "cardNumber";

    HistoryDetailsConverter(HttpUrl httpUrl, HtmlAdapter<HistoryDetails> htmlAdapter) {
        super(httpUrl, htmlAdapter);
    }

    @Override
    public HistoryDetails convert(ResponseBody responseBody) throws IOException {
        HistoryDetails details = super.convert(responseBody);
        for(Ride ride: details.getRides()){
            setField(ride, FIELD_MONTH, getMonth(ride.getDateTime()));
            setField(ride, FIELD_CARD_NUMBER, details.getCardNumber());
        }
        return details;
    }


}
