package me.krasnoyarsk.card.api.objects;

import java.util.List;

import me.krasnoyarsk.card.db.entities.MonthsStat;
import me.krasnoyarsk.card.db.entities.Payment;
import pl.droidsonroids.jspoon.annotation.Selector;

import static me.krasnoyarsk.card.api.util.ApiConstants.REGEX_NUMBER;

public class CardDetails {

    @Selector(value = ".main-col > p", regex = REGEX_NUMBER)
    private long cardNumber;

    @Selector(".main-col > table")
    private CurrentBalance balance;

    /**
     * tr~tr - all tr elements, preceded by another tr element.
     * i.e. all tr elements except for the first one, which contains the headers
     */
    @Selector(".main-col > table:nth-child(8) tr~tr")
    private List<Payment> payments;

    @Selector(".main-col > table:nth-child(11) tr~tr")
    private List<MonthsStat> history;

    private CardDetails(){}

    public CurrentBalance getBalance() {
        return balance;
    }

    public List<Payment> getPayments() {
        return payments;
    }

    public List<MonthsStat> getHistory() {
        return history;
    }

    public long getCardNumber() {
        return cardNumber;
    }

    @Override
    public String toString() {
        return "CardDetails{" +
                "balance=" + balance +
                ", payments=" + payments +
                ", history=" + history +
                '}';
    }
}
