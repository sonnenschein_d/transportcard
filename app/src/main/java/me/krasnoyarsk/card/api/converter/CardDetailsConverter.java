package me.krasnoyarsk.card.api.converter;

import java.io.IOException;
import java.util.Date;

import me.krasnoyarsk.card.api.objects.CardDetails;
import me.krasnoyarsk.card.db.entities.MonthsStat;
import me.krasnoyarsk.card.db.entities.Payment;
import okhttp3.HttpUrl;
import okhttp3.ResponseBody;
import pl.droidsonroids.jspoon.HtmlAdapter;

public class CardDetailsConverter extends AbstractConverter<CardDetails> {

    public static final String FIELD_CARD_NUMBER = "cardNumber";
    public static final String FIELD_RETRIEVED_DETAILS = "retrievedDetails";

    CardDetailsConverter(HttpUrl httpUrl, HtmlAdapter<CardDetails> htmlAdapter) {
        super(httpUrl, htmlAdapter);
    }

    @Override
    public CardDetails convert(ResponseBody responseBody) throws IOException {
        CardDetails details = super.convert(responseBody);
        for (MonthsStat month : details.getHistory()) {
            setField(month, FIELD_CARD_NUMBER, details.getCardNumber());
            setField(month, FIELD_RETRIEVED_DETAILS, new Date(0));
        }
        for (Payment payment: details.getPayments()){
            setField(payment, FIELD_CARD_NUMBER, details.getCardNumber());
        }
        return details;
    }
}
