package me.krasnoyarsk.card.api;

import io.reactivex.Observable;
import me.krasnoyarsk.card.api.util.ApiConstants.Type;
import me.krasnoyarsk.card.api.objects.CardDetails;
import me.krasnoyarsk.card.api.objects.HistoryDetails;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface KrasinformService {

    @FormUrlEncoded
    @POST("/card/{type}/")
    Observable<CardDetails> getCardDetails(
            @Field("card_num") String cardNumber,
            @Path("type") @Type String type
    );

    @GET("/{path}")
    Observable<HistoryDetails> getHistoryDetails(@Path("path") String path);
}
