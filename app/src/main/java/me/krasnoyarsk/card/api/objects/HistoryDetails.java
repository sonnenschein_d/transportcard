package me.krasnoyarsk.card.api.objects;

import java.util.List;

import me.krasnoyarsk.card.db.entities.Ride;
import pl.droidsonroids.jspoon.annotation.Selector;

import static me.krasnoyarsk.card.api.util.ApiConstants.REGEX_NUMBER;

public class HistoryDetails {

    @Selector(value = ".main-col > p", regex = REGEX_NUMBER)
    private long cardNumber;

    @Selector(".main-col > table:nth-child(5) tr~tr")
    private List<Ride> rides;

    private HistoryDetails(){}

    public List<Ride> getRides() {
        return rides;
    }

    public long getCardNumber() {
        return cardNumber;
    }

    @Override
    public String toString() {
        return "HistoryDetails{" +
                "rides=" + rides +
                '}';
    }
}
