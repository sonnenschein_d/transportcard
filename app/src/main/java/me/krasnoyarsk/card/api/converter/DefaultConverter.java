package me.krasnoyarsk.card.api.converter;

import okhttp3.HttpUrl;
import pl.droidsonroids.jspoon.HtmlAdapter;

public class DefaultConverter<T> extends AbstractConverter<T> {
    DefaultConverter(HttpUrl httpUrl, HtmlAdapter<T> htmlAdapter) {
        super(httpUrl, htmlAdapter);
    }
}
