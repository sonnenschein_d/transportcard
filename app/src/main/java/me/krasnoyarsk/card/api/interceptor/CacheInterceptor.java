package me.krasnoyarsk.card.api.interceptor;

import android.support.annotation.NonNull;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import me.krasnoyarsk.card.api.util.NetworkState;
import okhttp3.CacheControl;
import okhttp3.Interceptor;
import okhttp3.Response;

import static me.krasnoyarsk.card.api.util.ApiConstants.HEADER_CACHE_CONTROL;
import static me.krasnoyarsk.card.api.util.ApiConstants.HEADER_EXPIRES;
import static me.krasnoyarsk.card.api.util.ApiConstants.HEADER_PRAGMA;

public class CacheInterceptor implements Interceptor{

    private NetworkState state;

    public CacheInterceptor(NetworkState state) {
        this.state = state;
    }

    @Override
    public Response intercept(@NonNull Chain chain) throws IOException {
        Response response = chain.proceed(chain.request());

        CacheControl cacheControl;

        if (state.isConnected()) {
            cacheControl = new CacheControl.Builder()
                    .maxAge(60, TimeUnit.SECONDS)
                    .build();
        } else {
            cacheControl = new CacheControl.Builder()
                    .maxStale(7, TimeUnit.DAYS)
                    .build();
        }

        return response.newBuilder()
                .removeHeader(HEADER_PRAGMA)
                .removeHeader(HEADER_EXPIRES)
                .removeHeader(HEADER_CACHE_CONTROL)
                .header(HEADER_CACHE_CONTROL, cacheControl.toString())
                .build();
    }
}
