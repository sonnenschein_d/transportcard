package me.krasnoyarsk.card.api.util;

import android.support.annotation.StringDef;

import java.lang.annotation.Retention;

import static java.lang.annotation.RetentionPolicy.SOURCE;

public final class ApiConstants {

    public static final String HEADER_CACHE_CONTROL = "Cache-Control";
    public static final String HEADER_PRAGMA = "Pragma";
    public static final String HEADER_EXPIRES = "Expires";

    private ApiConstants(){throw new IllegalAccessError();}

    public static final String BASE_URL = "http://www.krasinform.ru/";

    public static final String TEST_CARD = "0100000005948737";

    public static final int CACHE_SIZE = 5 * 1024 * 1024; // 5 MB

    /*
    Длина номера транспортной карты
     */
    public static final int LENGTH_TRANSPORT = 16;

    public static final String TRANSPORT = "transport";
    public static final String SOCIAL = "social";
    public static final String UEK = "uek";
    public static final String KOPILKA = "kopilka";
    public static final String SCHOOL = "school";

    public static final String REGEX_NUMBER = "((?<=\\s|^)\\d+(?=\\s|$))";

    @Retention(SOURCE)
    @StringDef({
            TRANSPORT,
            SOCIAL,
            UEK,
            KOPILKA,
            SCHOOL
    })
    public @interface Type {}

}
