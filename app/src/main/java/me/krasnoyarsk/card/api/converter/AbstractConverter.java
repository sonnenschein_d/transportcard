package me.krasnoyarsk.card.api.converter;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.nio.charset.Charset;

import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.ResponseBody;
import pl.droidsonroids.jspoon.HtmlAdapter;
import retrofit2.Converter;

public abstract class AbstractConverter<T> implements Converter<ResponseBody, T> {

    private final HttpUrl httpUrl;
    private final HtmlAdapter<T> htmlAdapter;

    AbstractConverter(HttpUrl httpUrl, HtmlAdapter<T> htmlAdapter) {
        this.httpUrl = httpUrl;
        this.htmlAdapter = htmlAdapter;
    }

    @Override
    public T convert(ResponseBody responseBody) throws IOException {
        Charset charset = null;
        MediaType mediaType = responseBody.contentType();
        if (mediaType != null)
            charset = mediaType.charset();

        InputStream is = responseBody.byteStream();
        try {
            return htmlAdapter.fromInputStream(is, charset, httpUrl.url());
        } finally {
            is.close();
        }
    }

    void setField(Object object, String fieldName, Object value){
        try {
            Field field = object.getClass().getDeclaredField(fieldName);
            field.setAccessible(true);
            field.set(object, value);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }
}
