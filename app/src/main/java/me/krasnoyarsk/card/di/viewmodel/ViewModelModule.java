package me.krasnoyarsk.card.di.viewmodel;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;
import me.krasnoyarsk.card.ui.add.manually.AddManuallyViewModel;
import me.krasnoyarsk.card.ui.add.scan.ScanViewModel;
import me.krasnoyarsk.card.ui.cards.CardsViewModel;

@Module
public abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(CardsViewModel.class)
    abstract ViewModel bindCardsViewModel(CardsViewModel viewModel);

    @Binds
    @IntoMap
    @ViewModelKey(AddManuallyViewModel.class)
    abstract ViewModel bindAddManuallyViewModel(AddManuallyViewModel viewModel);

    @Binds
    @IntoMap
    @ViewModelKey(ScanViewModel.class)
    abstract ViewModel bindScanViewModel(ScanViewModel viewModel);

    @Binds
    abstract ViewModelProvider.Factory bindViewModelFactory(ViewModelFactory factory);
}
