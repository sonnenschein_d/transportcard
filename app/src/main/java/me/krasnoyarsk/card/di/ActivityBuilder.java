package me.krasnoyarsk.card.di;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import me.krasnoyarsk.card.ui.add.manually.AddManuallyActivity;
import me.krasnoyarsk.card.ui.add.manually.AddManuallyModule;
import me.krasnoyarsk.card.ui.add.scan.ScanActivity;
import me.krasnoyarsk.card.ui.add.scan.ScanModule;
import me.krasnoyarsk.card.ui.cards.CardsActivity;
import me.krasnoyarsk.card.ui.cards.CardsActivityModule;

@Module
public abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = CardsActivityModule.class)
    abstract CardsActivity bindCardsActivity();

    @ContributesAndroidInjector(modules = AddManuallyModule.class)
    abstract AddManuallyActivity bindAddManuallyActivity();

    @ContributesAndroidInjector(modules = ScanModule.class)
    abstract ScanActivity bindScanActivity();
}
