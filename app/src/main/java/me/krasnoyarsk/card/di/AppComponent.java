package me.krasnoyarsk.card.di;

import android.app.Application;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.DaggerApplication;
import dagger.android.support.AndroidSupportInjectionModule;
import me.krasnoyarsk.card.App;
import me.krasnoyarsk.card.api.NetworkModule;
import me.krasnoyarsk.card.db.DbModule;
import me.krasnoyarsk.card.di.viewmodel.ViewModelModule;
import me.krasnoyarsk.card.repository.task.AddReminderToCalendarIfNecessaryTask;
import me.krasnoyarsk.card.repository.task.FetchCardInfoTask;
import me.krasnoyarsk.card.ui.cards.CardsAdapter;

@Singleton
@Component(modules = {
        AndroidSupportInjectionModule.class,
        AppModule.class,
        ActivityBuilder.class,
        NetworkModule.class,
        DbModule.class,
        ViewModelModule.class})
public interface AppComponent extends AndroidInjector<DaggerApplication> {

    void inject(App app);
    void inject(FetchCardInfoTask task);
    void inject(AddReminderToCalendarIfNecessaryTask task);
    void inject(CardsAdapter adapter);

    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(Application application);

        AppComponent build();
    }

}
