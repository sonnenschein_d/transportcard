package me.krasnoyarsk.card.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Transaction;
import android.arch.persistence.room.TypeConverters;
import android.arch.persistence.room.Update;

import java.util.Date;
import java.util.List;

import io.reactivex.Flowable;
import me.krasnoyarsk.card.db.converters.DateTypeConverters;
import me.krasnoyarsk.card.db.entities.Card;

@Dao
abstract public class CardDao {

    /**
     * If we set the onConflict strategy to REPLACE,
     * the row will be first deleted on fetch & a new row will be inserted.
     * We don't want that because all the MonthsStat & Payment objects will be deleted in this case
     * on every fetch.
     *
     * We also want to keep the 'created' field unchanged
     * & it's not possible that the card will change it's type
     *
     * @param card
     */
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    abstract public long insert(Card card);

    @TypeConverters(DateTypeConverters.class)
    @Query("SELECT * FROM Card ORDER BY created")
    abstract public Flowable<List<Card>> getAll();

    @Query("SELECT * FROM Card WHERE number = :number")
    abstract public Flowable<List<Card>> getByNumber(long number);

    @Query("SELECT * FROM Card WHERE number = :number")
    abstract public Card getByNumberSync(long number);

    @TypeConverters(DateTypeConverters.class)
    @Query("UPDATE Card SET balance = :balance, lastUpdated = :lastUpdated WHERE number = :number")
    abstract void update(long number, int balance, Date lastUpdated);

    @Delete
    abstract public void delete(Card card);

    @Insert(onConflict = OnConflictStrategy.ROLLBACK)
    abstract public void insertFailIfExists(Card card);

    /**
     * Use it instead of insert method for inserting/updating card data
     * @param card
     */
    @Transaction
    public void upsert(Card card){
        long insertedRows = insert(card);
        if (insertedRows == -1) {
            update(card.number, card.balance, card.lastUpdated);
        }
    }

}
