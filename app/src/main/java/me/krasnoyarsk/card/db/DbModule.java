package me.krasnoyarsk.card.db;

import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import java.util.Locale;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import me.krasnoyarsk.card.db.callbacks.InitDbCallback;
import me.krasnoyarsk.card.db.dao.CardDao;
import me.krasnoyarsk.card.db.dao.MonthStatDao;
import me.krasnoyarsk.card.db.dao.PaymentDao;
import me.krasnoyarsk.card.db.dao.RideDao;

/**
 * Created by Ekaterina Trofimova on 31/03/2018.
 */
@Module
public class DbModule {

    public static final String ROOM_INIT = "init";

    @Singleton
    @Provides
    CardDb provideDb(Context context, @Named(ROOM_INIT) RoomDatabase.Callback init) {
        return Room
                .databaseBuilder(context, CardDb.class, "cards.db")
                //.addCallback(init)
                .build();
    }

    @Singleton
    @Provides
    @Named(ROOM_INIT)
    RoomDatabase.Callback provideRdc() {
        return new InitDbCallback();
    }

    @Singleton
    @Provides
    CardDao provideCardDao(CardDb db) {
        return db.cardDao();
    }

    @Singleton
    @Provides
    MonthStatDao provideMonthDao(CardDb db) {
        return db.monthDao();
    }

    @Singleton
    @Provides
    PaymentDao providePaymentDao(CardDb db) {
        return db.paymentDao();
    }

    @Singleton
    @Provides
    RideDao provideRideDao(CardDb db) {
        return db.rideDao();
    }

    @Provides
    Locale provideLocale() {
        return new Locale("ru", "ru");
    }

}
