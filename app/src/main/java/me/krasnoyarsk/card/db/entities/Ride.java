package me.krasnoyarsk.card.db.entities;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.TypeConverters;
import android.support.annotation.NonNull;

import java.util.Date;

import me.krasnoyarsk.card.db.converters.DateTypeConverters;
import pl.droidsonroids.jspoon.annotation.Format;
import pl.droidsonroids.jspoon.annotation.Selector;

/*
Детализированная информация о поездке, совершенной по карточке

    <tr>
        <td>12.04.2018 18:30:24</td>
        <td>Эл. кошелек</td>
        <td>63</td>
        <td>21</td>
    </tr>

 */
@Entity(primaryKeys = {"dateTime", "cardNumber"},
        indices = {@Index("month"), @Index("cardNumber")},
        foreignKeys = @ForeignKey(entity = MonthsStat.class,
            parentColumns = {"period", "cardNumber"},
            childColumns = {"month", "cardNumber"},
            onUpdate = ForeignKey.CASCADE,
            onDelete = ForeignKey.CASCADE,
            deferred = true))
@TypeConverters(DateTypeConverters.class)
public class Ride {

    /*
    Дата поездки
     */
    @NonNull
    @Format(value = "dd.MM.yyyy HH:mm:ss")
    @Selector("td:first-child")
    private Date dateTime;

    private Date month;

    @NonNull
    private long cardNumber;

    /*
    Тип поездки
     */
    @Selector("td:nth-child(2)")
    private String type;

    /*
    Номер маршрута
     */
    @Selector("td:nth-child(3)")
    private String route;

    /*
    Списано
     */
    @Selector("td:nth-child(4)")
    private int charged;

    private Ride(){}

    public Ride(@NonNull Date dateTime, String type, String route, int charged, Date month, @NonNull long cardNumber) {
        this.dateTime = dateTime;
        this.type = type;
        this.route = route;
        this.charged = charged;
        this.month = month;
        this.cardNumber = cardNumber;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public String getType() {
        return type;
    }

    public String getRoute() {
        return route;
    }

    public int getCharged() {
        return charged;
    }

    public Date getMonth() {
        return month;
    }

    @NonNull
    public long getCardNumber() {
        return cardNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Ride)) return false;

        Ride ride = (Ride) o;

        if (cardNumber != ride.cardNumber) return false;
        if (charged != ride.charged) return false;
        if (!dateTime.equals(ride.dateTime)) return false;
        if (month != null ? !month.equals(ride.month) : ride.month != null) return false;
        if (type != null ? !type.equals(ride.type) : ride.type != null) return false;
        return route != null ? route.equals(ride.route) : ride.route == null;
    }

    @Override
    public int hashCode() {
        int result = dateTime.hashCode();
        result = 31 * result + (month != null ? month.hashCode() : 0);
        result = 31 * result + (int) (cardNumber ^ (cardNumber >>> 32));
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (route != null ? route.hashCode() : 0);
        result = 31 * result + charged;
        return result;
    }

    @Override
    public String toString() {
        return "Ride{" +
                "dateTime='" + dateTime + '\'' +
                ", type='" + type + '\'' +
                ", route='" + route + '\'' +
                ", charged='" + charged + '\'' +
                '}';
    }
}
