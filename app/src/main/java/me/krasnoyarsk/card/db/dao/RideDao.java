package me.krasnoyarsk.card.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.TypeConverters;

import java.util.Date;
import java.util.List;

import io.reactivex.Flowable;
import me.krasnoyarsk.card.db.converters.DateTypeConverters;
import me.krasnoyarsk.card.db.entities.Ride;

@Dao
public interface RideDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(List<Ride> payments);

    @Query("SELECT * FROM Ride WHERE cardNumber = :number ORDER BY dateTime DESC")
    Flowable<List<Ride>> getByCard(long number);

    @TypeConverters(DateTypeConverters.class)
    @Query("SELECT * FROM Ride WHERE cardNumber = :number AND month = :month ORDER BY dateTime DESC")
    Flowable<List<Ride>> getByCardAndMonth(long number, Date month);

    @TypeConverters(DateTypeConverters.class)
    @Query("DELETE FROM Ride WHERE cardNumber = :number AND month = :month")
    void delete(long number, Date month);

}
