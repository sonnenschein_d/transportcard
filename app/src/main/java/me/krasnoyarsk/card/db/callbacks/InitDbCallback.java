package me.krasnoyarsk.card.db.callbacks;

import android.annotation.SuppressLint;
import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.RoomDatabase;

import java.util.Date;

public class InitDbCallback extends RoomDatabase.Callback {

    @SuppressLint("DefaultLocale")
    public void onCreate(SupportSQLiteDatabase db) {
        // prepopulate database for test purposes

        //seems that we cannot use transactions here
        //has something to do with checking the identity hash string in the room_master_table
        long timestamp = new Date().getTime();
        for (int i = 1; i < 10; i++) {
            db.execSQL(String.format("INSERT INTO Card (number, type, balance, lastUpdated, created) " +
                    "VALUES (%d, 'transport', 999, %d, %d);", i, timestamp, timestamp));
        }
    }

}
