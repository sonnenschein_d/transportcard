package me.krasnoyarsk.card.db.entities;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;
import android.support.annotation.NonNull;

import java.util.Date;

import me.krasnoyarsk.card.db.converters.DateTypeConverters;

@Entity
@TypeConverters(DateTypeConverters.class)
public class Card {

    @NonNull
    @PrimaryKey
    public final long number;
    public final String type;
    public final int balance;
    public final Date lastUpdated;
    public final Date created;

    public Card(long number, String type, int balance, Date lastUpdated, Date created) {
        this.number = number;
        this.type = type;
        this.balance = balance;
        this.lastUpdated = lastUpdated;
        this.created = created;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Card)) return false;

        Card card = (Card) o;

        if (number != card.number) return false;
        if (balance != card.balance) return false;
        if (type != null ? !type.equals(card.type) : card.type != null) return false;
        if (lastUpdated != null ? !lastUpdated.equals(card.lastUpdated) : card.lastUpdated != null)
            return false;
        return created != null ? created.equals(card.created) : card.created == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (number ^ (number >>> 32));
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + balance;
        result = 31 * result + (lastUpdated != null ? lastUpdated.hashCode() : 0);
        result = 31 * result + (created != null ? created.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Card{" +
                "number=" + number +
                ", type='" + type + '\'' +
                ", balance=" + balance +
                ", lastUpdated=" + lastUpdated +
                ", created=" + created +
                '}';
    }
}
