package me.krasnoyarsk.card.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Transaction;
import android.arch.persistence.room.TypeConverters;
import android.arch.persistence.room.Update;
import android.support.annotation.VisibleForTesting;

import java.util.Date;
import java.util.List;

import io.reactivex.Flowable;
import me.krasnoyarsk.card.db.converters.DateTypeConverters;
import me.krasnoyarsk.card.db.entities.MonthsStat;

@Dao
abstract public class MonthStatDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    abstract void insert(List<MonthsStat> months);

    @Update
    abstract void update(List<MonthsStat> months);

    @Query("SELECT * FROM MonthsStat WHERE cardNumber = :number ORDER BY period DESC")
    abstract public Flowable<List<MonthsStat>> getByCard(long number);

    @VisibleForTesting
    @Delete
    abstract void delete(MonthsStat stat);

    @Transaction
    public void upsert(List<MonthsStat> months){
        insert(months);
        update(months);
    }

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract public void insert(MonthsStat month);

    @TypeConverters(DateTypeConverters.class)
    @Query("SELECT * FROM MonthsStat WHERE cardNumber = :number AND period = :period LIMIT 1")
    abstract public MonthsStat getByCardAndMonth(long number, Date period);

}
