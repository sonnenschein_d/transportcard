package me.krasnoyarsk.card.db.util;

import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.inject.Singleton;

import me.krasnoyarsk.card.api.util.ApiConstants.Type;
import me.krasnoyarsk.card.db.entities.Card;
import me.krasnoyarsk.card.util.DateUtil;

@Singleton
public final class CardExtensions {

    private final Locale locale;

    @Inject
    CardExtensions(Locale locale){
        this.locale = locale;
    }

    public String numberAsString(Card card){
        return String.format(locale, "%016d", card.number);
    }

    public boolean isRecent(Card card){
        //the data about card balance is not updated too often.
        //Let's assume that updating the data in database once in an hour is sufficient
        return DateUtil.isRecent(card.lastUpdated, 1, TimeUnit.HOURS);
    }

    public Card cardFor(String number, @Type String type){
        return new Card(Long.valueOf(number), type, 0, new Date(0), new Date());
    }
}
