package me.krasnoyarsk.card.db.entities;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.TypeConverters;
import android.support.annotation.NonNull;

import java.util.Date;

import me.krasnoyarsk.card.db.converters.DateTypeConverters;
import pl.droidsonroids.jspoon.annotation.Format;
import pl.droidsonroids.jspoon.annotation.Selector;

/**
 * <tr>
     * <td>20.03.2018 18:56:16</td>
     * <td>9412261
        * <br>DB3BFE7275970993
     * </td>
     * <td>1000</td>
     * <td>Эл. кошелек
     * <br>1000 т.е.
     * <br>
     * </td>
     * <td>ТС Красноярск 80</td>
 * </tr>
 */
@Entity(primaryKeys = {"dateTime", "cardNumber"},
        indices = {@Index("cardNumber")},
        foreignKeys = @ForeignKey(entity = Card.class,
            parentColumns = {"number"},
            childColumns = {"cardNumber"},
            onUpdate = ForeignKey.CASCADE,
            onDelete = ForeignKey.CASCADE,
            deferred = true))
@TypeConverters(DateTypeConverters.class)
public class Payment {

    /*
    Дата и время пополнения
     */
    @NonNull
    @Format(value = "dd.MM.yyyy HH:mm:ss")
    @Selector("td:first-child")
    private Date dateTime;

    @NonNull
    private long cardNumber;

    /*
    Номер платежа/номер чека
     */
    @Selector("td:nth-child(2)")
    private String number;

    /*
    Стоимость пополнения
     */
    @Selector("td:nth-child(3)")
    private int value;

    /*
    Зачислено
     */
    @Selector("td:nth-child(4)")
    private String topUp;

    /*
    Приемщик
     */
    @Selector("td:nth-child(5)")
    private String receiver;

    public Payment(@NonNull Date dateTime, String number, int value, String topUp, String receiver, long cardNumber) {
        this.dateTime = dateTime;
        this.number = number;
        this.value = value;
        this.topUp = topUp;
        this.receiver = receiver;
        this.cardNumber = cardNumber;
    }

    //jspoon doesn't work without default constructor
    private Payment(){
    }

    public Date getDateTime() {
        return dateTime;
    }

    public String getNumber() {
        return number;
    }

    public int getValue() {
        return value;
    }

    public String getTopUp() {
        return topUp;
    }

    public String getReceiver() {
        return receiver;
    }

    public long getCardNumber() {
        return cardNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Payment)) return false;

        Payment payment = (Payment) o;

        if (cardNumber != payment.cardNumber) return false;
        if (value != payment.value) return false;
        if (!dateTime.equals(payment.dateTime)) return false;
        if (number != null ? !number.equals(payment.number) : payment.number != null) return false;
        if (topUp != null ? !topUp.equals(payment.topUp) : payment.topUp != null) return false;
        return receiver != null ? receiver.equals(payment.receiver) : payment.receiver == null;
    }

    @Override
    public int hashCode() {
        int result = dateTime.hashCode();
        result = 31 * result + (int) (cardNumber ^ (cardNumber >>> 32));
        result = 31 * result + (number != null ? number.hashCode() : 0);
        result = 31 * result + value;
        result = 31 * result + (topUp != null ? topUp.hashCode() : 0);
        result = 31 * result + (receiver != null ? receiver.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Payment{" +
                "dateTime='" + dateTime + '\'' +
                ", number='" + number + '\'' +
                ", value='" + value + '\'' +
                ", topUp='" + topUp + '\'' +
                ", receiver='" + receiver + '\'' +
                '}';
    }
}
