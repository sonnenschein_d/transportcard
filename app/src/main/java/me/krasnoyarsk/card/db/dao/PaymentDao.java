package me.krasnoyarsk.card.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import io.reactivex.Flowable;
import me.krasnoyarsk.card.db.entities.Payment;

@Dao
public interface PaymentDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(List<Payment> payments);

    @Query("SELECT * FROM Payment WHERE cardNumber = :number ORDER BY dateTime DESC")
    Flowable<List<Payment>> getByCard(long number);
}
