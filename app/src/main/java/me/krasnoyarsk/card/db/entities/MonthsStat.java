package me.krasnoyarsk.card.db.entities;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.TypeConverters;
import android.support.annotation.NonNull;

import java.util.Date;

import me.krasnoyarsk.card.db.converters.DateTypeConverters;
import pl.droidsonroids.jspoon.annotation.Format;
import pl.droidsonroids.jspoon.annotation.Selector;

/*
     <tr>
        <td rowspan="1">04.2018</td>
        <td>Эл. кошелек</td>
        <td>16</td>
        <td>336</td>
        <td rowspan="1">
            <a href="/card/transport/0100000005948737/04.2018/detail/">Детализация</a>
        </td>
    </tr>
 */
@Entity(primaryKeys = {"period", "cardNumber"},
        indices = {@Index("cardNumber")},
        foreignKeys = @ForeignKey(entity = Card.class,
            parentColumns = {"number"},
            childColumns = {"cardNumber"},
            onUpdate = ForeignKey.CASCADE,
            onDelete = ForeignKey.CASCADE,
            deferred = true))
@TypeConverters(DateTypeConverters.class)
public class MonthsStat {

    /*
    Период
     */
    @NonNull
    @Format(value = "MM.yyyy")
    @Selector("td:first-child")
    private Date period;

    @NonNull
    private long cardNumber;

    /*
    Тип проездного
     */
    @Selector("td:nth-child(2)")
    private String type;

    /*
    Количество
     */
    @Selector("td:nth-child(3)")
    private int quantity;

    /*
    Списано
     */
    @Selector("td:nth-child(4)")
    private int charged;

    /*
    Детализация
     */
    @Selector(value = "td:nth-child(5) a", attr = "href")
    private String detailsLink;

    private Date retrievedDetails;

    public MonthsStat(Date period, String type, int quantity, int charged, String detailsLink, long cardNumber, Date retrievedDetails) {
        this.period = period;
        this.type = type;
        this.quantity = quantity;
        this.charged = charged;
        this.detailsLink = detailsLink;
        this.cardNumber = cardNumber;
        this.retrievedDetails = retrievedDetails;
    }

    private MonthsStat() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MonthsStat)) return false;

        MonthsStat that = (MonthsStat) o;

        if (cardNumber != that.cardNumber) return false;
        if (quantity != that.quantity) return false;
        if (charged != that.charged) return false;
        if (!period.equals(that.period)) return false;
        if (type != null ? !type.equals(that.type) : that.type != null) return false;
        if (detailsLink != null ? !detailsLink.equals(that.detailsLink) : that.detailsLink != null)
            return false;
        return retrievedDetails != null ? retrievedDetails.equals(that.retrievedDetails) : that.retrievedDetails == null;
    }

    @Override
    public int hashCode() {
        int result = period.hashCode();
        result = 31 * result + (int) (cardNumber ^ (cardNumber >>> 32));
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + quantity;
        result = 31 * result + charged;
        result = 31 * result + (detailsLink != null ? detailsLink.hashCode() : 0);
        result = 31 * result + (retrievedDetails != null ? retrievedDetails.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "MonthsStat{" +
                "period=" + period +
                ", type='" + type + '\'' +
                ", quantity='" + quantity + '\'' +
                ", charged='" + charged + '\'' +
                ", detailsLink='" + detailsLink + '\'' +
                '}';
    }

    public Date getPeriod() {
        return period;
    }

    public String getType() {
        return type;
    }

    public int getQuantity() {
        return quantity;
    }

    public int getCharged() {
        return charged;
    }

    public String getDetailsLink() {
        return detailsLink;
    }

    public long getCardNumber() {
        return cardNumber;
    }

    public Date getRetrievedDetails() {
        return retrievedDetails;
    }
}
