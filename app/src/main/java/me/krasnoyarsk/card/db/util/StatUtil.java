package me.krasnoyarsk.card.db.util;

import java.util.Date;

import me.krasnoyarsk.card.db.entities.MonthsStat;

public final class StatUtil {

    private StatUtil(){throw new IllegalAccessError();}

    public static MonthsStat updateRetrievedDetailsDate(MonthsStat stat){
        return updateRetrievedDetailsDate(stat, new Date());
    }

    private static MonthsStat updateRetrievedDetailsDate(MonthsStat stat, Date retrievedDetails){
        return new MonthsStat(stat.getPeriod(),
                stat.getType(),
                stat.getQuantity(),
                stat.getCharged(),
                stat.getDetailsLink(),
                stat.getCardNumber(),
                retrievedDetails);
    }

}
