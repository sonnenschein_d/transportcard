package me.krasnoyarsk.card.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import me.krasnoyarsk.card.db.dao.CardDao;
import me.krasnoyarsk.card.db.dao.MonthStatDao;
import me.krasnoyarsk.card.db.dao.PaymentDao;
import me.krasnoyarsk.card.db.dao.RideDao;
import me.krasnoyarsk.card.db.entities.Card;
import me.krasnoyarsk.card.db.entities.MonthsStat;
import me.krasnoyarsk.card.db.entities.Payment;
import me.krasnoyarsk.card.db.entities.Ride;


@Database(entities = {Card.class, MonthsStat.class, Payment.class, Ride.class},
        version = 1)
public abstract class CardDb extends RoomDatabase {

    abstract public CardDao cardDao();
    abstract public MonthStatDao monthDao();
    abstract public PaymentDao paymentDao();
    abstract public RideDao rideDao();

}
