package me.krasnoyarsk.card.util.calendar;

public class AddToCalenderFailedException extends Exception {

    public AddToCalenderFailedException() {
    }

    public AddToCalenderFailedException(String message) {
        super(message);
    }

    public AddToCalenderFailedException(String message, Throwable cause) {
        super(message, cause);
    }

    public AddToCalenderFailedException(Throwable cause) {
        super(cause);
    }
}
