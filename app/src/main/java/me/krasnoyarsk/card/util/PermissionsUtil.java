package me.krasnoyarsk.card.util;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import java.util.ArrayList;
import java.util.List;

public final class PermissionsUtil {

    public static final int PERMISSION_REQUESTS = 1;

    private PermissionsUtil(){throw new IllegalAccessError();}

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    public static boolean isPermissionGranted(Context context, String permission) {
        return ContextCompat.checkSelfPermission(context, permission)
                == PackageManager.PERMISSION_GRANTED;
    }

    public static boolean allPermissionsGranted(Context context, String[] permissions) {
        for (String permission : permissions) {
            if (!isPermissionGranted(context, permission)) {
                return false;
            }
        }
        return true;
    }

    public static String[] getRequiredPermissions(Context context) {
        try {
            PackageInfo info =
                    context.getPackageManager()
                            .getPackageInfo(context.getPackageName(), PackageManager.GET_PERMISSIONS);
            String[] ps = info.requestedPermissions;
            if (ps != null && ps.length > 0) {
                return ps;
            } else {
                return new String[0];
            }
        } catch (Exception e) {
            return new String[0];
        }
    }

    public static void getRuntimePermissions(Activity activity) {
        List<String> allNeededPermissions = new ArrayList<>();
        for (String permission : getRequiredPermissions(activity)) {
            if (!isPermissionGranted(activity, permission)) {
                allNeededPermissions.add(permission);
            }
        }

        if (!allNeededPermissions.isEmpty()) {
            ActivityCompat.requestPermissions(
                    activity, allNeededPermissions.toArray(new String[0]), PERMISSION_REQUESTS);
        }
    }

    public static boolean allPermissionsGranted(Activity activity){
        return allPermissionsGranted(activity, getRequiredPermissions(activity));
    }

    public static void doIfPermissionsGrantedOrAskPermissions(Activity activity, Runnable doIt){
        if (allPermissionsGranted(activity)) {
            doIt.run();
        } else {
            getRuntimePermissions(activity);
        }
    }
}
