package me.krasnoyarsk.card.util;

import java.util.Date;

import me.krasnoyarsk.card.api.objects.CardDetails;
import me.krasnoyarsk.card.api.util.ApiConstants.Type;
import me.krasnoyarsk.card.db.entities.Card;

public final class Create {

    private Create(){throw new IllegalAccessError();}

    public static Card cardFrom(CardDetails details, @Type String type){
        return new Card(details.getCardNumber(),
                type,
                details.getBalance().getState(),
                new Date(),
                new Date());
    }

    public static Card cardFrom(long number, @Type String type){
        Date created = new Date();
        Date lastUpdated = DateUtil.addDays(new Date(), -1);
        return new Card(number, type, 0, lastUpdated, created);
    }
}
