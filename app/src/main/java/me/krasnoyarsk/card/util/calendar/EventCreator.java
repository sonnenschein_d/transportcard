package me.krasnoyarsk.card.util.calendar;

import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;

import java.util.Date;
import java.util.Objects;
import java.util.TimeZone;

public class EventCreator {

    private static final String EVENT_URI = "content://com.android.calendar/events";
    private static final String REMINDER_URI = "content://com.android.calendar/reminders";
    private static final String CALENDAR_ID = "calendar_id";
    private static final String TITLE = "title";
    private static final String DESCRIPTION = "description";
    private static final String START_DATE = "dtstart";
    private static final String END_DATE = "dtend";
    private static final String ALL_DAY = "allDay";
    private static final String EVENT_ID = "event_id";
    private static final String MINUTES_BEFORE_EVENT = "minutes";
    private static final String REMINDER_METHOD = "method";
    public static final String TIMEZONE = "eventTimezone";

    private final Context appContext;

    public EventCreator(Context context) {
        this.appContext = context.getApplicationContext();
    }

    public long addToCalender(Event event) throws AddToCalenderFailedException{
        try {
            return addEvent(event);
        } catch (Exception ex) {
            throw new AddToCalenderFailedException(ex);
        }
    }

    private long addEvent(Event event){
        ContentValues eventValues = new ContentValues();
        eventValues.put(CALENDAR_ID, 1); // id, We need to choose from
        // our mobile for primary its 1
        eventValues.put(TITLE, event.getTitle());
        eventValues.put(DESCRIPTION, event.getDescription());

        eventValues.put(START_DATE, event.getStartDate());
        eventValues.put(END_DATE, event.getEndDate());
        eventValues.put(ALL_DAY, event.isAllDay() ? 1 : 0);
        eventValues.put(TIMEZONE, TimeZone.getDefault().getDisplayName());

        //eventValues.put("hasAlarm", 1); // 0 for false, 1 for true

        Uri eventUri = appContext
                .getContentResolver()
                .insert(Uri.parse(EVENT_URI), eventValues);
        Objects.requireNonNull(eventUri);
        long eventID = Long.parseLong(Objects.requireNonNull(eventUri.getLastPathSegment()));
        if (event.needsReminder()){
            addReminder(eventID, event.getMinutesBeforeEventWhenRemind());
        }
        return eventID;
    }

    private void addReminder(long eventID, int minutesBeforeEvent){
        ContentValues reminderValues = new ContentValues();
        reminderValues.put(EVENT_ID, eventID);
        reminderValues.put(MINUTES_BEFORE_EVENT, minutesBeforeEvent);
        reminderValues.put(REMINDER_METHOD, 1); // Alert Methods: Default(0),
        // Alert(1), Email(2),SMS(3)
        Uri reminderUri = appContext
                .getContentResolver()
                .insert(Uri.parse(REMINDER_URI), reminderValues);
    }

}
