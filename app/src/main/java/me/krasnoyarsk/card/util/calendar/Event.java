package me.krasnoyarsk.card.util.calendar;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.Date;

public class Event {

    private static final long _10_MINUTES = 10L * 60L * 1000L;

    private final @NonNull String title;
    private final @NonNull String description;
    private final long startDate;
    private final long endDate;
    private final boolean allDay;
    private final boolean needsReminder;
    private final int minutesBeforeEventWhenRemind;

    private Event(Builder builder) {
        if (builder.endDate != null
                && builder.startDate.compareTo(builder.endDate) > 0){
            throw new IllegalArgumentException(builder.startDate + " after " + builder.endDate);
        }
        title = builder.title;
        description = builder.description;
        startDate = builder.startDate.getTime();
        endDate = builder.endDate != null ? builder.endDate.getTime() : builder.startDate.getTime() + _10_MINUTES;
        allDay = builder.allDay;
        needsReminder = builder.needsReminder;
        minutesBeforeEventWhenRemind = builder.minutesBeforeEventWhenRemind;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    @NonNull
    public String getTitle() {
        return title;
    }

    @NonNull
    public String getDescription() {
        return description;
    }

    public long getStartDate() {
        return startDate;
    }

    public long getEndDate() {
        return endDate;
    }

    public boolean isAllDay() {
        return allDay;
    }

    public boolean needsReminder() {
        return needsReminder;
    }

    public int getMinutesBeforeEventWhenRemind() {
        return minutesBeforeEventWhenRemind;
    }

    public static final class Builder {
        private String title;
        private String description;
        private Date startDate;
        private Date endDate;
        private boolean allDay;
        private boolean needsReminder;
        private int minutesBeforeEventWhenRemind;

        private Builder() {
        }

        public Builder title(@NonNull String val) {
            title = val;
            return this;
        }

        public Builder description(@NonNull String val) {
            description = val;
            return this;
        }

        public Builder startDate(@NonNull Date val) {
            startDate = val;
            return this;
        }

        public Builder endDate(@Nullable Date val) {
            endDate = val;
            return this;
        }

        public Builder allDay(boolean val) {
            allDay = val;
            return this;
        }

        public Builder needsReminder(boolean val) {
            needsReminder = val;
            return this;
        }

        public Builder minutesBeforeEventWhenRemind(int val) {
            minutesBeforeEventWhenRemind = val;
            return this;
        }

        public Event build() {
            return new Event(this);
        }
    }
}
