package me.krasnoyarsk.card.util;

import android.support.annotation.Nullable;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public final class DateUtil {

    public static final String DATE_TIME = "HH:mm:ss dd.MM.yyyy";
    public static final String DATE = "dd.MM.yyyy";
    public static final String MONTH = "MM.yyyy";

    //TODO: to make locale injectable by dagger
    private static final Locale locale = new Locale("ru", "ru");

    private DateUtil(){}

    public static Date fromString(String pattern, String value) throws ParseException{
        return new SimpleDateFormat(pattern, locale).parse(value);
    }

    public static @Nullable Date fromStringNoException(String pattern, String value){
        try {
            return new SimpleDateFormat(pattern,locale).parse(value);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static boolean isRecent(Date date, int time, TimeUnit unit){
        return new Date().getTime() - date.getTime() < unit.toMillis(time);
    }

    public static Date getMonth(Date date){
        try {
            SimpleDateFormat format = new SimpleDateFormat("MM.yyyy", locale);
            return format.parse(format.format(date));
        } catch (ParseException e) {
            throw new RuntimeException("Should never land here");
        }
    }

    public static Date addDays(Date date, int numberOfDays){
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, numberOfDays);
        return cal.getTime();
    }

    public static Date todaysOrTomorrowEvening(){
        Date now = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(now);
        int hour = cal.get(Calendar.HOUR_OF_DAY);
        cal.set(Calendar.HOUR_OF_DAY, 19);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        if (hour >= 19){
            //tomorrow
            cal.add(Calendar.DATE, 1);
        }
        return cal.getTime();
    }

}
