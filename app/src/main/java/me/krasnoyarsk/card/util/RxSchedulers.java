package me.krasnoyarsk.card.util;


import javax.inject.Inject;

import io.reactivex.FlowableTransformer;
import io.reactivex.ObservableTransformer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Based on http://blog.danlew.net/2015/03/02/dont-break-the-chain/
 *
 * Example of usage:
 *
 * private RxSchedulers schedulers;
 * private ObjectsRepository repository;
 *
 * void testMethod(){
 *      Flowable<SomeObject> flowable = repository
 *                                          .someObjects()
 *                                          .compose(schedulers.applyToFlowable());
 *
 *     Observable<OtherObject> observable =  repository
 *                                              .otherObjects()
 *                                              .compose(schedulers.applyToObservable());
 * }
 */
public class RxSchedulers {

    final ObservableTransformer observableSchedulers =
            observable -> observable
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread());

    final FlowableTransformer flowableSchedulers =
            flowable -> flowable
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread());

    @Inject
    public RxSchedulers(){}

    @SuppressWarnings("unchecked")
    public <T> ObservableTransformer<T, T> applyToObservable() {
        return (ObservableTransformer<T, T>) observableSchedulers;
    }

    @SuppressWarnings("unchecked")
    public <T> FlowableTransformer<T, T> applyToFlowable() {
        return (FlowableTransformer<T, T>) flowableSchedulers;
    }

}
