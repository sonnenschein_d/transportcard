package me.krasnoyarsk.card.repository.task;

import android.support.annotation.VisibleForTesting;

import io.reactivex.Observable;
import me.krasnoyarsk.card.db.entities.Card;

public class UpdateCardInfoTask extends Task {

    private FetchCardInfoTask task;

    @VisibleForTesting
    UpdateCardInfoTask(FetchCardInfoTask task){
        this.loading = null;
        this.task = task;
    }

    //TODO: update the task so that it runs FetchAndAddReminder instead of just Fetch task
    public static UpdateCardInfoTask create(Card card){
        FetchCardInfoTask task = FetchCardInfoTask.create(card);
        return new UpdateCardInfoTask(task);
    }

    @Override
    public Observable<Boolean> isLoading() {
        return task.loading;
    }

    @Override
    public void run() {
        boolean isRecent = task.extensions.isRecent(task.card);
        if (isRecent){
            task.loading.onComplete();
            return;
        }

        task.run();
    }
}
