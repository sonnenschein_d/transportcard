package me.krasnoyarsk.card.repository.task;

import android.Manifest.permission;
import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.RequiresPermission;
import android.support.annotation.VisibleForTesting;

import java.util.Date;

import javax.inject.Inject;

import me.krasnoyarsk.card.App;
import me.krasnoyarsk.card.R;
import me.krasnoyarsk.card.db.CardDb;
import me.krasnoyarsk.card.db.entities.Card;
import me.krasnoyarsk.card.util.DateUtil;
import me.krasnoyarsk.card.util.calendar.AddToCalenderFailedException;
import me.krasnoyarsk.card.util.calendar.Event;
import me.krasnoyarsk.card.util.calendar.EventCreator;
import timber.log.Timber;

import static me.krasnoyarsk.card.util.PermissionsUtil.isPermissionGranted;

public class AddReminderToCalendarIfNecessaryTask extends Task {

    protected final long number;
    protected final int limit;

    @VisibleForTesting
    AddReminderToCalendarIfNecessaryTask(Card card, int limit) {
        this.number = card.number;
        this.limit = limit;
    }

    public static AddReminderToCalendarIfNecessaryTask create(Card card, int limit) {
        AddReminderToCalendarIfNecessaryTask task = new AddReminderToCalendarIfNecessaryTask(card, limit);
        App.getAppComponent().inject(task);
        return task;
    }

    @Inject
    CardDb db;
    @Inject
    Context context;

    @SuppressLint("MissingPermission")
    @Override
    public void run() {
        loading.onNext(true);
        try {
            Card updated = db.cardDao().getByNumberSync(number);
            int balance = updated.balance;
            if (balance <= limit && isPermissionGranted(context, permission.WRITE_CALENDAR)){
                addReminder();
            }
            loading.onNext(false);
            loading.onComplete();
        } catch (Exception e) {
            Timber.e(e);
            loading.onError(e);
        }
    }

    @RequiresPermission(permission.WRITE_CALENDAR)
    private void addReminder(){
        try {
            EventCreator creator = new EventCreator(context);
            Date date = DateUtil.todaysOrTomorrowEvening();
            Event event = Event.newBuilder()
                    .title(context.getString(R.string.reminder_title))
                    .description(context.getString(R.string.reminder_message))
                    .startDate(date)
                    .endDate(date)
                    .needsReminder(true)
                    .minutesBeforeEventWhenRemind(0)
                    .build();
            creator.addToCalender(event);
        } catch (AddToCalenderFailedException e) {
            Timber.e(e);
        }
    }
}
