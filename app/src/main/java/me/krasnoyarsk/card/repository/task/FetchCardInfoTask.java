package me.krasnoyarsk.card.repository.task;

import android.annotation.SuppressLint;
import android.support.annotation.VisibleForTesting;

import javax.inject.Inject;

import me.krasnoyarsk.card.App;
import me.krasnoyarsk.card.api.KrasinformService;
import me.krasnoyarsk.card.db.CardDb;
import me.krasnoyarsk.card.db.entities.Card;
import me.krasnoyarsk.card.db.util.CardExtensions;
import me.krasnoyarsk.card.util.Create;
import timber.log.Timber;

public class FetchCardInfoTask extends Task {

    protected final Card card;

    @Inject
    CardDb db;

    @Inject
    KrasinformService service;

    @Inject
    CardExtensions extensions;

    @VisibleForTesting
    FetchCardInfoTask(Card card) {
        this.card = card;
    }

    public static FetchCardInfoTask create(Card card) {
        FetchCardInfoTask task = new FetchCardInfoTask(card);
        App.getAppComponent().inject(task);
        return task;
    }

    @SuppressLint("CheckResult")
    @Override
    public void run() {
        loading.onNext(true);
        final String type = card.type;
        service.getCardDetails(extensions.numberAsString(card), card.type)
                .subscribe(
                        details -> {
                            db.runInTransaction(() -> {
                                Card card = Create.cardFrom(details, type);
                                db.cardDao().upsert(card);
                                db.monthDao().upsert(details.getHistory());
                                db.paymentDao().insert(details.getPayments());
                            });
                            loading.onNext(false);
                        },
                        error -> {
                            loading.onNext(false);
                            loading.onError(error);
                            Timber.e(error);
                        },
                        loading::onComplete
                );
    }
}
