package me.krasnoyarsk.card.repository;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Flowable;
import me.krasnoyarsk.card.db.dao.MonthStatDao;
import me.krasnoyarsk.card.db.entities.MonthsStat;
import me.krasnoyarsk.card.repository.resource.Resource;

@Singleton
public class MonthsRepository {

    private MonthStatDao dao;

    @Inject
    public MonthsRepository(MonthStatDao dao) {
        this.dao = dao;
    }

    public Flowable<Resource<List<MonthsStat>>> monthsByCard(long number){
        return dao.getByCard(number).map(Resource::success);
    }
}
