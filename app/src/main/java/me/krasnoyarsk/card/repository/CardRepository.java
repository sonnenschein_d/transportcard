package me.krasnoyarsk.card.repository;

import android.support.annotation.VisibleForTesting;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import me.krasnoyarsk.card.api.KrasinformService;
import me.krasnoyarsk.card.db.dao.CardDao;
import me.krasnoyarsk.card.db.entities.Card;
import me.krasnoyarsk.card.db.util.CardExtensions;
import me.krasnoyarsk.card.repository.resource.Resource;
import me.krasnoyarsk.card.repository.task.FetchCardInfoTask;
import me.krasnoyarsk.card.repository.task.Task;
import me.krasnoyarsk.card.repository.task.UpdateCardInfoTask;
import me.krasnoyarsk.card.util.AppExecutors;

@Singleton
public class CardRepository {

    private KrasinformService service;
    private CardDao dao;
    private AppExecutors executors;
    private CardExtensions extensions;

    @VisibleForTesting
    UpdateTaskCreator updateTaskCreator = new UpdateTaskCreator();
    @VisibleForTesting
    FetchTaskCreator fetchTaskCreator = new FetchTaskCreator();

    @Inject
    public CardRepository(KrasinformService service,
                          CardDao dao,
                          AppExecutors executors,
                          CardExtensions extensions) {
        this.service = service;
        this.dao = dao;
        this.executors = executors;
        this.extensions = extensions;
    }

    public Flowable<Resource<List<Card>>> cards() {
        return dao.getAll().map(Resource::success);
    }

    public Observable<Boolean> fetch(Card card) {
        return doTask(fetchTaskCreator, card);
    }

    public Observable<Boolean> update(Card card) {
        return doTask(updateTaskCreator, card);
    }

    public Observable<Boolean> fetch(String number, String type) {
        return fetch(extensions.cardFor(number, type));
    }

    @VisibleForTesting
    Observable<Boolean> doTask(TaskCreator creator, Card card){
        Task task = creator.create(card);
        executors.getNetworkIO().execute(task);
        return task.isLoading();
    }

    interface TaskCreator{
        Task create(Card card);
    }

    static class FetchTaskCreator implements TaskCreator{
        @Override
        public Task create(Card card) {
            return FetchCardInfoTask.create(card);
        }
    }

    static class UpdateTaskCreator implements TaskCreator{
        @Override
        public Task create(Card card) {
            return UpdateCardInfoTask.create(card);
        }
    }

}
