package me.krasnoyarsk.card.repository;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Flowable;
import me.krasnoyarsk.card.db.dao.PaymentDao;
import me.krasnoyarsk.card.db.entities.Payment;
import me.krasnoyarsk.card.repository.resource.Resource;

@Singleton
public class PaymentRepository {

    private PaymentDao dao;

    @Inject
    public PaymentRepository(PaymentDao dao) {
        this.dao = dao;
    }

    public Flowable<Resource<List<Payment>>> paymentsByCard(long number){
        return dao.getByCard(number).map(Resource::success);
    }
}
