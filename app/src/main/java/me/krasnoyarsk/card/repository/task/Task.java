package me.krasnoyarsk.card.repository.task;

import io.reactivex.Observable;
import io.reactivex.subjects.BehaviorSubject;

public abstract class Task implements Runnable {

    protected BehaviorSubject<Boolean> loading = BehaviorSubject.create();

    public Observable<Boolean> isLoading(){
        return loading;
    }
}
