package me.krasnoyarsk.card.repository;

import android.support.annotation.VisibleForTesting;

import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.FlowableEmitter;
import io.reactivex.Observable;
import io.reactivex.functions.Function;
import me.krasnoyarsk.card.api.KrasinformService;
import me.krasnoyarsk.card.api.objects.HistoryDetails;
import me.krasnoyarsk.card.db.CardDb;
import me.krasnoyarsk.card.db.entities.MonthsStat;
import me.krasnoyarsk.card.db.entities.Ride;
import me.krasnoyarsk.card.db.util.StatUtil;
import me.krasnoyarsk.card.repository.resource.NetworkBoundListResource;
import me.krasnoyarsk.card.repository.resource.Resource;
import me.krasnoyarsk.card.util.DateUtil;

@Singleton
public class RideRepository {

    private KrasinformService service;
    private CardDb db;

    @Inject
    public RideRepository(KrasinformService service, CardDb db) {
        this.service = service;
        this.db = db;
    }

    public Flowable<Resource<List<Ride>>> ridesFor(MonthsStat stat){
        return Flowable.create(
                emitter -> RidesResource.create(emitter, stat, service, db),
                BackpressureStrategy.BUFFER
        );
    }

    static class RidesResource extends NetworkBoundListResource<List<Ride>, HistoryDetails>{

        static RidesResource create(FlowableEmitter<Resource<List<Ride>>> emitter,
                                    MonthsStat stat,
                                    KrasinformService service,
                                    CardDb db){
            RidesResource res = new RidesResource(emitter, stat, service, db);
            res.start();
            return res;
        }

        private final MonthsStat stat;
        private final KrasinformService service;
        private final CardDb db;

        @VisibleForTesting
        RidesResource(FlowableEmitter<Resource<List<Ride>>> emitter,
                             MonthsStat stat,
                             KrasinformService service,
                             CardDb db) {
            super(emitter);
            this.stat = stat;
            this.service = service;
            this.db = db;
        }

        @Override
        protected Observable<HistoryDetails> getRemote() {
            return service.getHistoryDetails(stat.getDetailsLink());
        }

        @Override
        protected Flowable<List<Ride>> getLocal() {
            return db.rideDao().getByCardAndMonth(stat.getCardNumber(), stat.getPeriod());
        }

        @Override
        protected void saveCallResult(List<Ride> data) {
            db.runInTransaction(() -> {
                db.monthDao().insert(StatUtil.updateRetrievedDetailsDate(stat));
                db.rideDao().insert(data);
            });
        }

        @Override
        protected Function<HistoryDetails, List<Ride>> mapper() {
            return HistoryDetails::getRides;
        }

        @Override
        protected boolean maybeStale(List<Ride> data) {
            return RideRepository.maybeStale(stat, db);
        }
    }

    //TODO: move to some util class
    private static boolean maybeStale(MonthsStat stat, CardDb db){
        return maybeStale(stat, db, new Date());
    }

    @VisibleForTesting
    static boolean maybeStale(MonthsStat stat, CardDb db, Date now){
        if (notChangedAnymore(stat, now)){
            return false;
        }
        return !fresh(stat, db, now);
    }

    /*
         Let's assume that the last changes for the previous month data
         can be done during the first week of the current month.
         If we are retrieving data for one of the previous months & more than 40 days have passed since the startForResult of that month
         than there can be no more fresh data for it & making additional calls to the server does not really make sense
     */
    static boolean notChangedAnymore(MonthsStat stat, Date now){
        return DateUtil.addDays(stat.getPeriod(), 40).before(now);
    }

    static boolean fresh(MonthsStat stat, CardDb db, Date now){
        MonthsStat updated = db.monthDao().getByCardAndMonth(stat.getCardNumber(), stat.getPeriod());
        return now.getTime() - updated.getRetrievedDetails().getTime() < TimeUnit.HOURS.toMillis(1);
    }


}
