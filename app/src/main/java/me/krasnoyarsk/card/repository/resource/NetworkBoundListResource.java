package me.krasnoyarsk.card.repository.resource;

import android.annotation.SuppressLint;
import android.support.annotation.VisibleForTesting;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.FlowableEmitter;
import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

/*
We want to fetch data from the network only if
1. There is no local data
2. There is local data, but it is stale
 */
public abstract class NetworkBoundListResource<LocalType extends List<?>, RemoteType> {

    private FlowableEmitter<Resource<LocalType>> emitter;

    public NetworkBoundListResource(FlowableEmitter<Resource<LocalType>> emitter) {
        this.emitter = emitter;
    }

    protected void start(){
        fetch(emitter);
    }

    @VisibleForTesting
    Scheduler scheduler = Schedulers.io();

    @VisibleForTesting
    @SuppressLint("CheckResult")
    void fetch(FlowableEmitter<Resource<LocalType>> emitter){
        getLocal()
                .subscribeOn(scheduler)
                .observeOn(scheduler)
                .map(localType -> {
                    if (localType.isEmpty()
                            || maybeStale(localType)) {
                        //no local data or stale local data
                        fetchRemote(emitter);
                        return Resource.loading(localType);
                    }
                    //no need to load remote data
                    return Resource.success(localType);
                })
                .subscribe(emitter::onNext, emitter::onError);
    }

    @VisibleForTesting
    @SuppressLint("CheckResult")
    void fetchRemote(FlowableEmitter<Resource<LocalType>> emitter) {
        getRemote()
                .retry(2)
                .take(1)
                .map(mapper())
                .subscribe(
                        this::saveCallResult,
                        error -> {
                            emitter.onNext(Resource.error(error.getLocalizedMessage(), null));
                            Timber.e(error);
                        },
                        () -> {
                            Timber.d("Fetching remote - onComplete!");
                        });

    }

    protected abstract Observable<RemoteType> getRemote();

    protected abstract Flowable<LocalType> getLocal();

    protected abstract void saveCallResult(LocalType data);

    protected abstract Function<RemoteType, LocalType> mapper();

    protected abstract boolean maybeStale(LocalType data);
}
