package me.krasnoyarsk.card.repository.task;

import android.support.annotation.VisibleForTesting;

import io.reactivex.disposables.Disposable;
import me.krasnoyarsk.card.db.entities.Card;

public class FetchAndAddReminderTask extends Task {

    private static final int DEFAULT_LIMIT = 200;

    private final FetchCardInfoTask fetch;
    private final AddReminderToCalendarIfNecessaryTask addReminder;

    @VisibleForTesting
    FetchAndAddReminderTask(FetchCardInfoTask fetch, AddReminderToCalendarIfNecessaryTask addReminder) {
        this.fetch = fetch;
        this.addReminder = addReminder;
    }

    public static FetchAndAddReminderTask create(Card card) {
        FetchCardInfoTask fetch = FetchCardInfoTask.create(card);
        AddReminderToCalendarIfNecessaryTask addReminder = AddReminderToCalendarIfNecessaryTask.create(card, DEFAULT_LIMIT);
        return new FetchAndAddReminderTask(fetch, addReminder);
    }

    @Override
    public void run() {
        this.loading.onNext(true);
        fetch.run();
        Disposable disposable = fetch.loading.subscribe(
                loading -> {
                    if (!loading) {
                        runAddReminder();
                    }
                },
                e -> this.loading.onError(e)
        );
    }

    private void runAddReminder() {
        addReminder.run();
        Disposable disposable = addReminder.loading.subscribe(
            loading -> {
                if (!loading){
                    this.loading.onNext(false);
                }
            },
            e -> this.loading.onError(e)
        );
    }
}
