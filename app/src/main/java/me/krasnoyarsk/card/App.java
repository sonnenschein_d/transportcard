package me.krasnoyarsk.card;

import com.squareup.leakcanary.LeakCanary;

import dagger.android.AndroidInjector;
import dagger.android.DaggerApplication;
import me.krasnoyarsk.card.di.AppComponent;
import me.krasnoyarsk.card.di.DaggerAppComponent;
import timber.log.Timber;
import timber.log.Timber.DebugTree;

public class App extends DaggerApplication {

    private static AppComponent appComponent;

    public static AppComponent getAppComponent(){
        return appComponent;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        initLeakCanary();
        initTimber();
    }

    private void initLeakCanary(){
        if (LeakCanary.isInAnalyzerProcess(this)) {
            // This process is dedicated to LeakCanary for heap analysis.
            // You should not init your app in this process.
            return;
        }
        LeakCanary.install(this);
    }

    private void initTimber(){
        if (BuildConfig.DEBUG){
            Timber.plant(new DebugTree());
        }
    }

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        AppComponent appComponent = DaggerAppComponent.builder().application(this).build();
        appComponent.inject(this);
        App.appComponent = appComponent;
        return appComponent;
    }
}
