package me.krasnoyarsk.card.ui.common.view;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.util.AttributeSet;
import android.widget.LinearLayout;

@CoordinatorLayout.DefaultBehavior(MoveUpwardBehaviour.class)
public class CoordinatorLinearLayout extends LinearLayout {

    public CoordinatorLinearLayout(Context context) {
        super(context);
    }

    public CoordinatorLinearLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public CoordinatorLinearLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

}
