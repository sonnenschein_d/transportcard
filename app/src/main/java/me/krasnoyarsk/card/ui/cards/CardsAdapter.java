package me.krasnoyarsk.card.ui.cards;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import javax.inject.Inject;

import me.krasnoyarsk.card.App;
import me.krasnoyarsk.card.R;
import me.krasnoyarsk.card.databinding.ItemCardBinding;
import me.krasnoyarsk.card.db.entities.Card;
import me.krasnoyarsk.card.db.util.CardExtensions;
import me.krasnoyarsk.card.ui.common.DataBoundListAdapter;
import me.krasnoyarsk.card.util.Objects;

public class CardsAdapter extends DataBoundListAdapter<Card, ItemCardBinding>{

    private final @NonNull CardCallback callback;
    private final @NonNull CardExtensions extensions;

    @Inject
    public CardsAdapter(@NonNull CardCallback callback, @NonNull CardExtensions extensions){
        this.callback = callback;
        this.extensions = extensions;
    }

    @Override
    protected ItemCardBinding createBinding(ViewGroup parent) {
        return DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()), R.layout.item_card,
                        parent, false);
    }

    @Override
    protected void bind(ItemCardBinding binding, Card item) {
        binding.setCard(item);
        binding.setExtension(extensions);
        binding.executePendingBindings();
        binding.getRoot().setOnClickListener(v -> {
            callback.onClick(item);
        });
        callback.updateIfNecessary(binding, item);
    }

    @Override
    protected boolean areItemsTheSame(Card oldItem, Card newItem) {
        return Objects.equals(oldItem.number, newItem.number);
    }

    @Override
    protected boolean areContentsTheSame(Card oldItem, Card newItem) {
        return oldItem.balance == newItem.balance
                && Objects.equals(oldItem.lastUpdated, newItem.lastUpdated);
    }

    public interface CardCallback {
        void onClick(Card card);
        void updateIfNecessary(ItemCardBinding binding, Card card);
    }
}
