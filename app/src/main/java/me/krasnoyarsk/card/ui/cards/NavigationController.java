package me.krasnoyarsk.card.ui.cards;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;

import javax.inject.Inject;

import me.krasnoyarsk.card.ui.add.manually.AddManuallyActivity;
import me.krasnoyarsk.card.ui.add.scan.ScanActivity;
import timber.log.Timber;

import static me.krasnoyarsk.card.ui.add.scan.ScanActivity.EXTRA_BARCODE;

/**
 * Created by Ekaterina Trofimova on 29/03/2018.
 */
public class NavigationController {

    private final Context context;

    @Inject
    public NavigationController(CardsActivity activity) {
        this.context = activity;
    }

    public void goToAddManually(){
        AddManuallyActivity.start(context);
    }

    public void goToScanCard(){
        ScanActivity.startForResult((Activity) context);
    }

    public void onResult(int requestCode, int resultCode, @Nullable Intent data){
        if (requestCode == ScanActivity.REQUEST_SCAN_BARCODE
                && resultCode == Activity.RESULT_OK
                && data != null){
            String barcode = data.getStringExtra(EXTRA_BARCODE);
            AddManuallyActivity.start(context, barcode);
        }
    }
}
