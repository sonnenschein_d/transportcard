package me.krasnoyarsk.card.ui.add.manually;

import android.arch.lifecycle.ViewModel;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import javax.inject.Inject;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observables.ConnectableObservable;
import io.reactivex.schedulers.Schedulers;
import me.krasnoyarsk.card.db.dao.CardDao;
import me.krasnoyarsk.card.db.entities.Card;
import me.krasnoyarsk.card.util.Create;

import static me.krasnoyarsk.card.api.util.ApiConstants.TRANSPORT;
import static me.krasnoyarsk.card.ui.add.manually.AddManuallyActivity.EXTRA_BARCODE;

public class AddManuallyViewModel extends ViewModel {

    private static final String CARD_NUMBER = "card_number";

    private CardNumber number;
    private CardDao dao;
    private ConnectableObservable<Boolean> saveConnectable;

    @Inject
    public AddManuallyViewModel(CardDao dao) {
        this.dao = dao;
    }

    public void onSaveInstanceState(Bundle out) {
        if (number != null) {
            out.putParcelable(CARD_NUMBER, number);
        }
    }

    public void onRestoreInstanceState(Bundle state, Intent intent) {
        if (number != null) {
            return;
        }

        if (state == null
                || (number = state.getParcelable(CARD_NUMBER)) == null) {
            number = new CardNumber(getNumberFromIntent(intent));
        }
        setSaveConnectable();
    }

    private String getNumberFromIntent(@Nullable Intent intent) {
        if (intent == null){
            return null;
        }
        return intent.getStringExtra(EXTRA_BARCODE);
    }

    private void setSaveConnectable() {
        saveConnectable = Single.just(number)
                .subscribeOn(Schedulers.io())
                .doOnSuccess(number -> {
                    Card card = Create.cardFrom(number.getNumberAsLong(), TRANSPORT);
                    dao.insert(card);
                })
                .map(__ -> true)
                .observeOn(AndroidSchedulers.mainThread())
                .toObservable()
                .publish();
    }

    public CardNumber getNumber() {
        return number;
    }

    public ConnectableObservable<Boolean> getSaveConnectable() {
        return saveConnectable;
    }

    public void save() {
        if (saveConnectable != null) {
            saveConnectable.connect();
        }
    }
}
