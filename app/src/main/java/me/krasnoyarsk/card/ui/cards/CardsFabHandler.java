package me.krasnoyarsk.card.ui.cards;

import android.support.v4.view.ViewCompat;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.OvershootInterpolator;

import java.lang.ref.WeakReference;

import javax.inject.Inject;

import me.krasnoyarsk.card.R;
import timber.log.Timber;

public class CardsFabHandler {

    private WeakReference<CardsActivity> activity;
    private boolean isMenuOpened;

    private Animation fabOpenAnimation;
    private Animation fabCloseAnimation;

    @Inject
    public CardsFabHandler(CardsActivity activity){
        this.activity = new WeakReference<>(activity);
        this.isMenuOpened = false;

        fabOpenAnimation = AnimationUtils.loadAnimation(activity, R.anim.fab_open);
        fabCloseAnimation = AnimationUtils.loadAnimation(activity, R.anim.fab_close);
    }

    public void onMenuFabClicked(View view){
        if (isMenuOpened){
            collapseMenu();
        } else {
            expandMenu();
        }
    }

    private void expandMenu() {
        isMenuOpened = true;
        rotateFabBaseWithAngle(45.0F);
        animateSubmenu(fabOpenAnimation);
        enableSubmenu(true);
        makeSubmenuFabsClickable(true);
    }

    private void collapseMenu() {
        isMenuOpened = false;
        rotateFabBaseWithAngle(0.0F);
        animateSubmenu(fabCloseAnimation);
        enableSubmenu(false);
        makeSubmenuFabsClickable(false);
    }

    private void rotateFabBaseWithAngle(float angle){
        CardsActivity activity = this.activity.get();
        ViewCompat
                .animate(activity.getBinding().fabmenu.fabBase)
                .rotation(angle)
                .withLayer()
                .setDuration(300)
                .setInterpolator(new OvershootInterpolator(10.0F))
                .start();
    }

    private void animateSubmenu(Animation animation){
        CardsActivity activity = this.activity.get();
        activity.getBinding().fabmenu.manuallyLayout.startAnimation(animation);
        activity.getBinding().fabmenu.scanQrCodeLayout.startAnimation(animation);
    }

    private void makeSubmenuFabsClickable(boolean clickable){
        CardsActivity activity = this.activity.get();
        activity.getBinding().fabmenu.fabScanBarcode.setClickable(clickable);
        activity.getBinding().fabmenu.fabAddManually.setClickable(clickable);
    }

    private void enableSubmenu(boolean enable){
        CardsActivity activity = this.activity.get();
        activity.getBinding().fabmenu.manuallyLayout.setEnabled(enable);
        activity.getBinding().fabmenu.scanQrCodeLayout.setEnabled(enable);
    }

    public void onAddManuallyClicked(View view){
        CardsActivity activity = this.activity.get();
        collapseMenu();
        activity.navigator.goToAddManually();
    }

    public void onAddViaBarcodeScanClicked(View view){
        CardsActivity activity = this.activity.get();
        collapseMenu();
        activity.navigator.goToScanCard();
    }

    /**
     * @return true if back pressed event was consumed
     */
    public boolean onBackPressed(){
        if (isMenuOpened){
            collapseMenu();
            return true;
        }
        return false;
    }
}
