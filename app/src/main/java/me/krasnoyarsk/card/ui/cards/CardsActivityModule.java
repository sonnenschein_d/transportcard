package me.krasnoyarsk.card.ui.cards;

import dagger.Binds;
import dagger.Module;
import me.krasnoyarsk.card.ui.cards.CardsAdapter.CardCallback;

@Module
public abstract class CardsActivityModule {

    @Binds
    abstract CardCallback provideCardCallback(CardsActivity activity);

    //TODO: !!
}
