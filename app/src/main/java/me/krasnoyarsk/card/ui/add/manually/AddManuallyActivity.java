package me.krasnoyarsk.card.ui.add.manually;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import me.krasnoyarsk.card.R;
import me.krasnoyarsk.card.databinding.ActivityAddManuallyBinding;
import me.krasnoyarsk.card.ui.DisposingDaggerActivity;
import timber.log.Timber;

public class AddManuallyActivity extends DisposingDaggerActivity<ActivityAddManuallyBinding, AddManuallyViewModel> {

    static final String EXTRA_BARCODE = "barcode";

    public static void start(Context context){
        start(context, null);
    }

    public static void start(Context context, @Nullable String barcode){
        Intent intent = new Intent(context, AddManuallyActivity.class);
        if (barcode != null){
            intent.putExtra(EXTRA_BARCODE, barcode);
        }
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setToolbar();
        setNumber(savedInstanceState);
        setSaveListener();

        subscribeToSaveResult();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        viewModel.onSaveInstanceState(outState);
    }

    @Override
    protected Class<AddManuallyViewModel> getViewModelClass() {
        return AddManuallyViewModel.class;
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_add_manually;
    }

    private void setToolbar() {
        setSupportActionBar(binding.toolbarView.toolbar);
    }

    private void setNumber(Bundle savedInstanceState){
        viewModel.onRestoreInstanceState(savedInstanceState, getIntent());
        binding.content.setNumber(viewModel.getNumber());
    }

    private void setSaveListener(){
        binding.content.save.setOnClickListener(view -> {
            viewModel.save();
        });
    }

    private void subscribeToSaveResult() {
        disposableManager.add(
                viewModel.getSaveConnectable()
                        .doAfterTerminate(this::finish)
                        .subscribe(
                                bool -> {},
                                error -> {
                                    Timber.e(error);
                                    showError(R.string.error_failed_to_save_card);
                                })
        );
    }
}
