package me.krasnoyarsk.card.ui.cards;

import android.arch.lifecycle.ViewModel;

import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import io.reactivex.subjects.BehaviorSubject;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.Subject;
import me.krasnoyarsk.card.databinding.ItemCardBinding;
import me.krasnoyarsk.card.db.entities.Card;
import me.krasnoyarsk.card.repository.CardRepository;
import me.krasnoyarsk.card.repository.resource.Resource;
import me.krasnoyarsk.card.util.RxSchedulers;

public class CardsViewModel extends ViewModel {

    private CardRepository repository;
    private RxSchedulers schedulers;

    private Flowable<Resource<List<Card>>> cards;
    private Subject<Boolean> isLoading = BehaviorSubject.create();
    private Subject<Throwable> errors = PublishSubject.create();

    @Inject
    public CardsViewModel(CardRepository repository, RxSchedulers schedulers) {
        this.repository = repository;
        this.schedulers = schedulers;

        cards = repository
                .cards()
                .compose(schedulers.applyToFlowable());
    }

    public Flowable<Resource<List<Card>>> getCards() {
        return cards;
    }

    public Observable<Boolean> getLoading() {
        return isLoading.compose(schedulers.applyToObservable());
    }

    public Observable<Throwable> getErrors() {
        return errors
                .debounce(1, TimeUnit.SECONDS)
                .compose(schedulers.applyToObservable());
    }

    public Disposable updateCardIfNecessary(ItemCardBinding binding, Card card){
        return repository.update(card)
                .compose(schedulers.applyToObservable())
                .subscribe(
                        binding::setIsLoading,
                        error -> {
                            binding.setIsLoading(false);
                            errors.onNext(error);
                        }
                );
    }
}
