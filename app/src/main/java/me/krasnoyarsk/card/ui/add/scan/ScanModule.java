package me.krasnoyarsk.card.ui.add.scan;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class ScanModule {

    @Binds
    abstract ScanCallback provideScanCallback(ScanActivity activity);
}
