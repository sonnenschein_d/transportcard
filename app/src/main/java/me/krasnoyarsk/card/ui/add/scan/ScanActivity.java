package me.krasnoyarsk.card.ui.add.scan;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat.OnRequestPermissionsResultCallback;

import java.io.IOException;

import javax.inject.Inject;

import me.krasnoyarsk.card.R;
import me.krasnoyarsk.card.databinding.ActivityScanBinding;
import me.krasnoyarsk.card.ui.DisposingDaggerActivity;
import me.krasnoyarsk.card.ui.add.scan.camera.BarcodeScanningProcessor;
import me.krasnoyarsk.card.ui.add.scan.camera.CameraSource;
import timber.log.Timber;

import static me.krasnoyarsk.card.util.PermissionsUtil.allPermissionsGranted;
import static me.krasnoyarsk.card.util.PermissionsUtil.doIfPermissionsGrantedOrAskPermissions;

public class ScanActivity extends DisposingDaggerActivity<ActivityScanBinding, ScanViewModel>
        implements OnRequestPermissionsResultCallback, ScanCallback{

    public static final int REQUEST_SCAN_BARCODE = 1;
    public static final String EXTRA_BARCODE = "barcode";

    public static void startForResult(Activity activity){
        Intent intent = new Intent(activity, ScanActivity.class);
        activity.startActivityForResult(intent, REQUEST_SCAN_BARCODE);
    }

    @Inject
    BarcodeScanningProcessor barcodeScanningProcessor;

    private CameraSource cameraSource = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbar();

        doIfPermissionsGrantedOrAskPermissions(this, this::createCameraSource);
    }

    private void createCameraSource() {
        // If there's no existing cameraSource, create one.
        if (cameraSource == null) {
            cameraSource = new CameraSource(this, binding.content.fireFaceOverlay);
        }
        cameraSource.setMachineLearningFrameProcessor(barcodeScanningProcessor);
    }

    /**
     * Starts or restarts the camera source, if it exists. If the camera source doesn't exist yet
     * (e.g., because onResume was called before the camera source was created), this will be called
     * again when the camera source is created.
     */
    private void startCameraSource() {
        if (cameraSource != null) {
            try {
                binding.content.firePreview.start(cameraSource, binding.content.fireFaceOverlay);
            } catch (IOException e) {
                Timber.e(e, "Unable to startForResult camera source.");
                cameraSource.release();
                cameraSource = null;
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        startCameraSource();
    }

    /** Stops the camera. */
    @Override
    protected void onPause() {
        super.onPause();
        binding.content.firePreview.stop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (cameraSource != null) {
            cameraSource.release();
        }
    }

    @Override
    public void onRequestPermissionsResult(
            int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (allPermissionsGranted(this)) {
            createCameraSource();
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void setToolbar() {
        setSupportActionBar(binding.toolbarView.toolbar);
    }

    @Override
    protected Class<ScanViewModel> getViewModelClass() {
        return ScanViewModel.class;
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_scan;
    }

    @Override
    public void onScanned(String result) {
        binding.content.firePreview.stop();
        Intent intent = new Intent();
        intent.putExtra(EXTRA_BARCODE, result);
        setResult(RESULT_OK, intent);
        finish();
    }
}
