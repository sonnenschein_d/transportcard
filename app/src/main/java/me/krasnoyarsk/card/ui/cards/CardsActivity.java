package me.krasnoyarsk.card.ui.cards;

import android.Manifest;
import android.Manifest.permission;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresPermission;
import android.support.design.widget.Snackbar;

import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import me.krasnoyarsk.card.R;
import me.krasnoyarsk.card.databinding.ActivityCardsBinding;
import me.krasnoyarsk.card.databinding.ItemCardBinding;
import me.krasnoyarsk.card.db.entities.Card;
import me.krasnoyarsk.card.repository.resource.Status;
import me.krasnoyarsk.card.ui.DisposingDaggerActivity;
import me.krasnoyarsk.card.ui.cards.CardsAdapter.CardCallback;
import me.krasnoyarsk.card.ui.common.view.SpaceItemDecoration;
import me.krasnoyarsk.card.util.DateUtil;
import me.krasnoyarsk.card.util.PermissionsUtil;
import me.krasnoyarsk.card.util.calendar.AddToCalenderFailedException;
import me.krasnoyarsk.card.util.calendar.Event;
import me.krasnoyarsk.card.util.calendar.EventCreator;
import timber.log.Timber;

import static me.krasnoyarsk.card.util.PermissionsUtil.allPermissionsGranted;
import static me.krasnoyarsk.card.util.PermissionsUtil.doIfPermissionsGrantedOrAskPermissions;
import static me.krasnoyarsk.card.util.PermissionsUtil.getRuntimePermissions;


public class CardsActivity extends DisposingDaggerActivity<ActivityCardsBinding, CardsViewModel> implements CardCallback {

    @Inject
    NavigationController navigator;
    @Inject
    CardsFabHandler fabHandler;
    @Inject
    CardsAdapter adapter;

    private Parcelable rvState;

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(BUNDLE_RECYCLER_LAYOUT, binding.content.rvCards.getLayoutManager().onSaveInstanceState());
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        rvState = savedInstanceState.getParcelable(BUNDLE_RECYCLER_LAYOUT);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setToolbar();
        setList();
        setFabHandler();

        subscribeToErrors();
        subscribeToCards();

        getRuntimePermissions(this);
    }

    private void setToolbar() {
        setSupportActionBar(binding.toolbarView.toolbar);
    }

    private void setList() {
        binding.content.rvCards.setAdapter(adapter);
        binding.content.rvCards.addItemDecoration(new SpaceItemDecoration(0, 60));
    }

    private void setFabHandler() {
        binding.setFabHandler(fabHandler);
        binding.executePendingBindings();
    }

    private void subscribeToErrors() {
        disposableManager.add(
                viewModel.getErrors()
                        .subscribe(error -> {
                            showError(getString(R.string.failed_to_update_data), Snackbar.LENGTH_LONG);
                        })
        );
    }

    private void subscribeToCards() {
        disposableManager.add(
                viewModel.getCards()
                        .subscribe(resource -> {
                                    binding.setIsLoading(resource.status == Status.LOADING);

                                    if (resource.status == Status.SUCCESS) {
                                        List<Card> cards = resource.data;
                                        binding.setResultCount(cards == null ? 0 : cards.size());
                                        adapter.replace(cards);
                                        if (rvState != null) {
                                            binding.content.rvCards.getLayoutManager().onRestoreInstanceState(rvState);
                                            rvState = null;
                                        }
                                    }

                                    binding.executePendingBindings();
                                },
                                error -> {
                                    showError(error.getLocalizedMessage());
                                })
        );

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        navigator.onResult(requestCode, resultCode, data);
    }

    @Override
    public void onClick(Card card) {
        //TODO: go to card details screen
        Timber.d(card.toString());
    }

    @Override
    public void updateIfNecessary(ItemCardBinding binding, Card card) {
        this.disposableManager.add(
                viewModel.updateCardIfNecessary(binding, card)
        );
    }

    @Override
    public void onBackPressed() {
        if (fabHandler.onBackPressed()) {
            return;
        }
        super.onBackPressed();
    }

    @Override
    protected Class<CardsViewModel> getViewModelClass() {
        return CardsViewModel.class;
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_cards;
    }
}