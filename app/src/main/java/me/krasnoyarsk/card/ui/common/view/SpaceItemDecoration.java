package me.krasnoyarsk.card.ui.common.view;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.View;

/**
 * Created by Ekaterina.Trofimova on 12-Dec-16.
 *
 * This ItemDecoration can be added to RecyclerView to create
 * additional empty space after the last element in the recycler view.
 *
 * This may be useful if we have a FAB element on the screen with
 * the recycler view & the last element of rv can't be properly
 * seen because of the FAB.
 */
public class SpaceItemDecoration extends RecyclerView.ItemDecoration{

    private int space;
    private int bottomSpace = 0;

    public SpaceItemDecoration(int spaceDp, int bottomSpaceDp) {
        this.space = spaceDp;
        this.bottomSpace = bottomSpaceDp;
    }

    public SpaceItemDecoration(int spaceDp) {
        this.space = spaceDp;
        this.bottomSpace = 0;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view,
                               RecyclerView parent, RecyclerView.State state) {

        final int spaceAbsolute = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, space, parent.getResources()
                .getDisplayMetrics());
        final int bottomSpaceAbsolute = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, bottomSpace, parent.getResources()
                .getDisplayMetrics());

        int childCount = parent.getChildCount();
        final int itemPosition = parent.getChildAdapterPosition(view);
        final int itemCount = state.getItemCount();

        outRect.left = spaceAbsolute;
        outRect.right = spaceAbsolute;
        outRect.bottom = spaceAbsolute;
        outRect.top = spaceAbsolute;

        if (itemCount > 0 && itemPosition == itemCount - 1) {
            outRect.bottom = bottomSpaceAbsolute;
        }
    }

}
