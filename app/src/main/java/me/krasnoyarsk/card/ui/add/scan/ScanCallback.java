package me.krasnoyarsk.card.ui.add.scan;

public interface ScanCallback {

    void onScanned(String result);
}
