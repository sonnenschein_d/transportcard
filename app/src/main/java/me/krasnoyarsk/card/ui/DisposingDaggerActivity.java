package me.krasnoyarsk.card.ui;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;

import javax.inject.Inject;

import dagger.android.support.DaggerAppCompatActivity;

public abstract class DisposingDaggerActivity<BindingType extends ViewDataBinding, ViewModelType extends ViewModel>
        extends DaggerAppCompatActivity {

    public static final String BUNDLE_RECYCLER_LAYOUT = "recycler_view_layout";

    protected ViewModelType viewModel;
    protected BindingType binding;

    @Inject
    protected DisposableManager disposableManager;
    @Inject
    protected ViewModelProvider.Factory viewModelFactory;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, getLayoutRes());
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(getViewModelClass());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        disposableManager.dispose();
    }

    protected void showError(String msg){
        showError(msg, Snackbar.LENGTH_INDEFINITE);
    }

    protected void showError(@StringRes int res){
        showError(getString(res));
    }

    protected void showError(String msg, int duration) {
        Snackbar
                .make(binding.getRoot(), msg, duration)
                .show();
    }

    protected abstract Class<ViewModelType> getViewModelClass();

    @LayoutRes
    protected abstract int getLayoutRes();

    public BindingType getBinding() {
        return binding;
    }
}
