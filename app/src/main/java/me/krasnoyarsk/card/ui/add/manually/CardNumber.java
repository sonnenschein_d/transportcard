package me.krasnoyarsk.card.ui.add.manually;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.os.Parcel;
import android.os.Parcelable;
import me.krasnoyarsk.card.BR;

import static me.krasnoyarsk.card.api.util.ApiConstants.LENGTH_TRANSPORT;

public class CardNumber extends BaseObservable implements Parcelable {

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public CardNumber createFromParcel(Parcel in) {
            return new CardNumber(in);
        }

        public CardNumber[] newArray(int size) {
            return new CardNumber[size];
        }
    };

    private String number;

    public CardNumber(){}

    public CardNumber(String number){
        if (number == null){
            this.number = null;
        } else if (number.length() <= LENGTH_TRANSPORT){
            this.number = number;
        } else {
            this.number = number.substring(0, LENGTH_TRANSPORT);
        }
    }

    private CardNumber(Parcel in) {
        number = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(number);
    }

    @Bindable
    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
        notifyPropertyChanged(BR.number);
        notifyPropertyChanged(BR.saveEnabled);
    }

    @Bindable
    public boolean getSaveEnabled(){
        return number != null
                && number.length() <= LENGTH_TRANSPORT
                && isValidNumber();
    }

    private boolean isValidNumber(){
        try {
            Long.valueOf(number);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public long getNumberAsLong(){
        if (number == null){
            return -1L;
        }
        return Long.valueOf(number);
    }
}
