package me.krasnoyarsk.card.db.dao;

import android.arch.persistence.room.Room;
import android.support.test.InstrumentationRegistry;

import org.junit.After;
import org.junit.Before;

import me.krasnoyarsk.card.db.CardDb;

abstract public class DbTest {

    protected CardDb db;

    @Before
    public void initDb() {
        db = Room.inMemoryDatabaseBuilder(InstrumentationRegistry.getContext(),
                CardDb.class).build();
    }

    @After
    public void closeDb() {
        db.close();
    }
}
