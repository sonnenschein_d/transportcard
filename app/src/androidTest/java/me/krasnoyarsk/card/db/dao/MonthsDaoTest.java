package me.krasnoyarsk.card.db.dao;

import android.database.sqlite.SQLiteConstraintException;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import me.krasnoyarsk.card.db.entities.Card;
import me.krasnoyarsk.card.db.entities.MonthsStat;
import me.krasnoyarsk.card.db.entities.Ride;
import me.krasnoyarsk.card.test.util.AssertRx;

import static me.krasnoyarsk.card.test.util.TestData.NUMBER1;
import static me.krasnoyarsk.card.test.util.TestData.card;
import static me.krasnoyarsk.card.test.util.TestData.laterStat;
import static me.krasnoyarsk.card.test.util.TestData.ride;
import static me.krasnoyarsk.card.test.util.TestData.stat;
import static me.krasnoyarsk.card.test.util.TestData.statUpdated;
import static org.junit.Assert.assertEquals;

@RunWith(AndroidJUnit4.class)
public class MonthsDaoTest extends DbTest {

    @Test
    public void insertAndRead() throws ParseException {
        Card card = card();
        List<MonthsStat> months = Collections.singletonList(stat());

        db.runInTransaction(() -> {
            db.cardDao().upsert(card);
            db.monthDao().insert(months);
        });

        AssertRx.singleResult(db.monthDao().getByCard(card.number), months);
    }

    @Test(expected = SQLiteConstraintException.class)
    public void insertForNonExistentCard() throws ParseException {
        List<MonthsStat> months = Collections.singletonList(stat());
        db.monthDao().insert(months);
    }

    @Test
    public void readForNonExistentCard(){
        AssertRx.noResultList(db.monthDao().getByCard(NUMBER1));
    }

    @Test
    public void foreignKeysUntouchedOnListUpdate() throws ParseException {
        Card card = card();
        List<MonthsStat> months = Collections.singletonList(stat());
        List<Ride> rides = Collections.singletonList(ride());

        db.runInTransaction(() -> {
            db.cardDao().upsert(card);
            db.monthDao().upsert(months);
            db.rideDao().insert(rides);
        });

        List<MonthsStat> updatedMonths = Collections.singletonList(statUpdated());
        db.monthDao().upsert(updatedMonths);

        AssertRx.singleResult(db.monthDao().getByCard(card.number), updatedMonths);
        AssertRx.singleResult(db.rideDao().getByCardAndMonth(card.number, months.get(0).getPeriod()), rides);
    }

    @Test
    public void foreignKeysDeletedOnDelete() throws ParseException{
        Card card = card();
        List<MonthsStat> months = Collections.singletonList(stat());
        List<Ride> rides = Collections.singletonList(ride());

        db.runInTransaction(() -> {
            db.cardDao().upsert(card);
            db.monthDao().upsert(months);
            db.rideDao().insert(rides);
        });

        db.monthDao().delete(months.get(0));

        AssertRx.noResultList(db.monthDao().getByCard(card.number));
        AssertRx.noResultList(db.rideDao().getByCardAndMonth(card.number, months.get(0).getPeriod()));
    }

    @Test
    public void foreignKeysDeletedOnSingleElementInsert() throws ParseException{
        Card card = card();
        List<MonthsStat> months = Collections.singletonList(stat());
        List<Ride> rides = Collections.singletonList(ride());

        db.runInTransaction(() -> {
            db.cardDao().upsert(card);
            db.monthDao().upsert(months);
            db.rideDao().insert(rides);
        });

        MonthsStat updatedMonth = statUpdated();
        db.monthDao().insert(updatedMonth);

        AssertRx.singleResult(db.monthDao().getByCard(card.number), Collections.singletonList(updatedMonth));
        AssertRx.noResultList(db.rideDao().getByCardAndMonth(card.number, months.get(0).getPeriod()));
    }

    @Test
    public void monthsRetrievedInDescOrder() throws ParseException{
        Card card = card();
        MonthsStat s1 = stat();
        MonthsStat s2 = laterStat(s1);
        List<MonthsStat> stats = new ArrayList<MonthsStat>() {{
            add(s1);
            add(s2);
        }};

        db.runInTransaction(() -> {
            db.cardDao().upsert(card);
            db.monthDao().insert(stats);
        });

        AssertRx.singleResult(
                db.monthDao().getByCard(card.number),
                new ArrayList<MonthsStat>() {{
                    add(s2);
                    add(s1);
                }});
    }

    @Test
    public void getByCardAndMonth() throws ParseException{
        Card card = card();
        MonthsStat stat = stat();
        List<MonthsStat> months = Collections.singletonList(stat);

        db.runInTransaction(() -> {
            db.cardDao().upsert(card);
            db.monthDao().upsert(months);
        });

        MonthsStat found = db.monthDao().getByCardAndMonth(card.number, stat.getPeriod());
        assertEquals(stat, found);
    }

}
