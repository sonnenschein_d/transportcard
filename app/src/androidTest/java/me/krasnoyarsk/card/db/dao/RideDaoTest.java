package me.krasnoyarsk.card.db.dao;

import android.database.sqlite.SQLiteConstraintException;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import me.krasnoyarsk.card.db.entities.Card;
import me.krasnoyarsk.card.db.entities.MonthsStat;
import me.krasnoyarsk.card.db.entities.Ride;
import me.krasnoyarsk.card.test.util.AssertRx;
import me.krasnoyarsk.card.util.DateUtil;

import static me.krasnoyarsk.card.test.util.TestData.NUMBER1;
import static me.krasnoyarsk.card.test.util.TestData.card;
import static me.krasnoyarsk.card.test.util.TestData.laterRide;
import static me.krasnoyarsk.card.test.util.TestData.ride;
import static me.krasnoyarsk.card.test.util.TestData.rideWithEmptyRoute;
import static me.krasnoyarsk.card.test.util.TestData.stat;
import static me.krasnoyarsk.card.util.DateUtil.MONTH;

@RunWith(AndroidJUnit4.class)
public class RideDaoTest extends DbTest {

    @Test
    public void insertAndRead() throws ParseException {
        Card card = card();
        List<MonthsStat> stats = Collections.singletonList(stat());
        List<Ride> rides = Collections.singletonList(ride());

        db.runInTransaction(() -> {
            db.cardDao().upsert(card);
            db.monthDao().insert(stats);
            db.rideDao().insert(rides);
        });

        AssertRx.singleResult(db.rideDao().getByCard(card.number), rides);
        AssertRx.singleResult(db.rideDao().getByCardAndMonth(card.number, stats.get(0).getPeriod()), rides);
    }

    @Test
    public void insertRideWithEmptyRoute() throws ParseException {
        Card card = card();
        List<MonthsStat> stats = Collections.singletonList(stat());
        List<Ride> rides = Collections.singletonList(rideWithEmptyRoute());

        db.runInTransaction(() -> {
            db.cardDao().upsert(card);
            db.monthDao().insert(stats);
            db.rideDao().insert(rides);
        });

        AssertRx.singleResult(db.rideDao().getByCard(card.number), rides);
        AssertRx.singleResult(db.rideDao().getByCardAndMonth(card.number, stats.get(0).getPeriod()), rides);
    }

    @Test
    public void readForNonExistentCard() {
        AssertRx.noResultList(db.rideDao().getByCard(NUMBER1));
    }

    @Test
    public void readForNonExistentMonth() throws ParseException {
        Card card = card();
        List<MonthsStat> stats = Collections.singletonList(stat());
        List<Ride> rides = Collections.singletonList(rideWithEmptyRoute());

        db.runInTransaction(() -> {
            db.cardDao().upsert(card);
            db.monthDao().insert(stats);
            db.rideDao().insert(rides);
        });

        AssertRx.noResultList(db.rideDao().getByCardAndMonth(card.number, DateUtil.fromString(MONTH, "03.2018")));
    }

    @Test(expected = SQLiteConstraintException.class)
    public void insertForNonExistentCard() throws ParseException {
        List<Ride> rides = Collections.singletonList(ride());
        db.rideDao().insert(rides);
    }

    @Test(expected = SQLiteConstraintException.class)
    public void insertForNonExistentMonth() throws ParseException {
        Card card = card();
        db.cardDao().upsert(card);

        List<Ride> rides = Collections.singletonList(ride());
        db.rideDao().insert(rides);
    }

    @Test
    public void deleteAllForMonth() throws ParseException {
        Card card = card();
        List<MonthsStat> stats = Collections.singletonList(stat());
        List<Ride> rides = Collections.singletonList(ride());

        db.runInTransaction(() -> {
            db.cardDao().upsert(card);
            db.monthDao().insert(stats);
            db.rideDao().insert(rides);
        });

        db.rideDao().delete(card.number, stats.get(0).getPeriod());

        AssertRx.noResultList(db.rideDao().getByCardAndMonth(card.number, stats.get(0).getPeriod()));
    }

    @Test
    public void ridesReadInDescOrder() throws ParseException {
        Card card = card();
        List<MonthsStat> stats = Collections.singletonList(stat());
        Ride r1 = ride();
        Ride r2 = laterRide(r1);
        List<Ride> rides = new ArrayList<Ride>() {{
            add(r1);
            add(r2);
        }};

        db.runInTransaction(() -> {
            db.cardDao().upsert(card);
            db.monthDao().insert(stats);
            db.rideDao().insert(rides);
        });

        AssertRx.singleResult(
                db.rideDao().getByCardAndMonth(card.number, stats.get(0).getPeriod()),
                new ArrayList<Ride>() {{
                    add(r2);
                    add(r1);
                }});
    }
}
