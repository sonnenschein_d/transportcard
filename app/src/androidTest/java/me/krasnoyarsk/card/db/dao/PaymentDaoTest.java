package me.krasnoyarsk.card.db.dao;

import android.database.sqlite.SQLiteConstraintException;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import me.krasnoyarsk.card.db.entities.Card;
import me.krasnoyarsk.card.db.entities.Payment;
import me.krasnoyarsk.card.test.util.AssertRx;

import static me.krasnoyarsk.card.test.util.TestData.NUMBER1;
import static me.krasnoyarsk.card.test.util.TestData.card;
import static me.krasnoyarsk.card.test.util.TestData.laterPayment;
import static me.krasnoyarsk.card.test.util.TestData.payment;

@RunWith(AndroidJUnit4.class)
public class PaymentDaoTest extends DbTest {

    @Test
    public void insertAndRead() throws ParseException {
        Card card = card();
        List<Payment> payments = Collections.singletonList(payment());

        db.runInTransaction(() -> {
            db.cardDao().upsert(card);
            db.paymentDao().insert(payments);
        });

        AssertRx.singleResult(db.paymentDao().getByCard(card.number), payments);
    }

    @Test
    public void readForNonExistentCard() {
        AssertRx.noResultList(db.paymentDao().getByCard(NUMBER1));
    }

    @Test(expected = SQLiteConstraintException.class)
    public void insertForNonExistentCard() throws ParseException {
        List<Payment> payments = Collections.singletonList(payment());
        db.paymentDao().insert(payments);
    }

    @Test
    public void paymentsReadInDescOrder() throws ParseException {
        Card card = card();
        Payment p1 = payment();
        Payment p2 = laterPayment(p1);
        List<Payment> payments = new ArrayList<Payment>() {{
            add(p1);
            add(p2);
        }};

        db.runInTransaction(() -> {
            db.cardDao().upsert(card);
            db.paymentDao().insert(payments);
        });

        AssertRx.singleResult(
                db.paymentDao().getByCard(card.number),
                new ArrayList<Payment>() {{
                    add(p2);
                    add(p1);
                }});
    }
}
