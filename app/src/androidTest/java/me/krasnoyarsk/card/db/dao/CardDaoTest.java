package me.krasnoyarsk.card.db.dao;

import android.support.test.runner.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.text.ParseException;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import me.krasnoyarsk.card.db.entities.Card;
import me.krasnoyarsk.card.db.entities.MonthsStat;
import me.krasnoyarsk.card.db.entities.Payment;
import me.krasnoyarsk.card.test.util.AssertRx;
import me.krasnoyarsk.card.util.DateUtil;

import static me.krasnoyarsk.card.test.util.TestData.LAST_UPDATED;
import static me.krasnoyarsk.card.test.util.TestData.NUMBER1;
import static me.krasnoyarsk.card.test.util.TestData.NUMBER2;
import static me.krasnoyarsk.card.test.util.TestData.card;
import static me.krasnoyarsk.card.test.util.TestData.cards;
import static me.krasnoyarsk.card.test.util.TestData.payment;
import static me.krasnoyarsk.card.test.util.TestData.stat;
import static me.krasnoyarsk.card.test.util.TestData.updatedCard;
import static me.krasnoyarsk.card.util.DateUtil.DATE_TIME;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

@RunWith(AndroidJUnit4.class)
public class CardDaoTest extends DbTest {

    @SuppressWarnings({"unchecked"})
    @Test
    public void insertCardAndRead() {
        Card card = card();
        db.cardDao().insert(card);

        AssertRx.singleResultList(db.cardDao().getByNumber(card.number), card);
    }

    @Test
    public void readWhenNothingInserted() {
        AssertRx.noResultList(db.cardDao().getAll());
    }

    @Test
    public void insertSeveralCardsAndReadAll() {
        List<Card> cards = cards(2);
        db.cardDao().upsert(cards.get(0));
        db.cardDao().upsert(cards.get(1));

        AssertRx.singleResult(db.cardDao().getAll(), cards);
    }

    @Test
    public void readCardThatDoesNotExist() {
        Card card = card();
        db.cardDao().insert(card);

        AssertRx.noResultList(db.cardDao().getByNumber(NUMBER2));
    }

    @Test
    public void formatNumber() {
        String expected = "0100000005948737";
        String actual = String.format("%016d", NUMBER1);
        assertThat(actual, is(expected));
    }

    @Test
    public void foreignKeysNotDeletedOnUpdate() throws ParseException {
        //create a card & a payment for this card
        Card card = card();
        Payment payment = payment();
        MonthsStat stat = stat();
        //store to the db
        db.runInTransaction(() -> {
            db.cardDao().upsert(card);
            db.paymentDao().insert(Collections.singletonList(payment));
            db.monthDao().insert(Collections.singletonList(stat));
        });

        //try to fetch now the card
        Date updated = DateUtil.fromString(DATE_TIME, "17:58:00 19.04.2018");
        Card updatedCard = updatedCard(card, 445, updated);
        db.cardDao().upsert(card);

        //check that no changes to payment are done
        AssertRx.singleResultList(db.paymentDao().getByCard(card.number), payment);

        //check that no changes to month stat are done
        AssertRx.singleResultList(db.monthDao().getByCard(card.number), stat);
    }

    @Test
    public void insertAndDelete() {
        Card card = card();
        db.cardDao().insert(card);
        db.cardDao().delete(card);

        AssertRx.noResultList(db.cardDao().getByNumber(card.number));
    }

    @Test
    public void foreignKeysDeletedOnDelete() throws ParseException {
        //create a card & a payment for this card
        Card card = card();
        Payment payment = payment();
        MonthsStat stat = stat();

        //store to the db
        db.runInTransaction(() -> {
            db.cardDao().upsert(card);
            db.paymentDao().insert(Collections.singletonList(payment));
            db.monthDao().insert(Collections.singletonList(stat));
        });

        //delete the card
        db.cardDao().delete(card);

        //check that payments for this card are deleted
        AssertRx.noResultList(db.paymentDao().getByCard(card.number));
        AssertRx.noResultList(db.monthDao().getByCard(card.number));
    }

    @Test
    public void updateBalance() {
        //insert card
        Card card = card();
        db.cardDao().upsert(card);

        //balance updated
        int newBalance = card.balance + 200;
        Date updated = LAST_UPDATED;
        card = updatedCard(card, newBalance, updated);
        db.cardDao().upsert(card);

        //check that balance is updated correctly but created field not changed
        AssertRx.singleResultList(db.cardDao().getByNumber(card.number),
                new Card(card.number, card.type, newBalance, updated, card.created));
    }
}
