package me.krasnoyarsk.card.test.util;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;
import io.reactivex.subscribers.TestSubscriber;

public final class AssertRx {

    private AssertRx(){throw new IllegalAccessError();}

    public static <T> void singleResult(Flowable<T> flowable, T result){
        TestSubscriber<T> subscriber = new TestSubscriber<>();
        flowable.subscribe(subscriber);
        subscriber.awaitTerminalEvent(2, TimeUnit.SECONDS);

        subscriber.assertNoErrors();
        subscriber.assertValue(result);
        subscriber.dispose();
    }

    public static <T> void singleResult(Observable<T> observable, T result){
        TestObserver<T> subscriber = new TestObserver<>();
        observable.subscribe(subscriber);
        subscriber.awaitTerminalEvent(2, TimeUnit.SECONDS);

        subscriber.assertNoErrors();
        subscriber.assertValue(result);
        subscriber.dispose();
    }

    public static <T> void singleResultList(Flowable<List<T>> flowable, T result){
        TestSubscriber<List<T>> subscriber = new TestSubscriber<>();
        flowable.subscribe(subscriber);
        subscriber.awaitTerminalEvent(2, TimeUnit.SECONDS);

        subscriber.assertNoErrors();
        subscriber.assertValue(Collections.singletonList(result));
        subscriber.dispose();
    }

    public static <T> void noResultList(Flowable<List<T>> flowable){
        TestSubscriber<List<T>> subscriber = new TestSubscriber<>();
        flowable.subscribe(subscriber);
        subscriber.awaitTerminalEvent(2, TimeUnit.SECONDS);

        subscriber.assertNoErrors();
        subscriber.assertValue(Collections.emptyList());
        subscriber.dispose();
    }
}
