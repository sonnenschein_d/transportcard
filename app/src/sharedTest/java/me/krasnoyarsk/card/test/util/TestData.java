package me.krasnoyarsk.card.test.util;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import me.krasnoyarsk.card.db.entities.Card;
import me.krasnoyarsk.card.db.entities.MonthsStat;
import me.krasnoyarsk.card.db.entities.Payment;
import me.krasnoyarsk.card.db.entities.Ride;
import me.krasnoyarsk.card.util.DateUtil;

import static me.krasnoyarsk.card.util.DateUtil.DATE_TIME;
import static me.krasnoyarsk.card.util.DateUtil.MONTH;
import static me.krasnoyarsk.card.util.DateUtil.addDays;

public final class TestData {

    public static final long NUMBER1 = 100000005948737L;
    public static final String TYPE = "transport";
    public static final int BALANCE = 198;
    public static final Date LAST_UPDATED = DateUtil.fromStringNoException(DATE_TIME, "14:08:00 19.04.2018");
    public static final Date CREATED = DateUtil.fromStringNoException(DATE_TIME, "12:00:00 19.04.2018");
    public static final long NUMBER2 = 100000005948738L;

    private TestData(){throw new IllegalAccessError();}

    public static Card card(){
        return card(NUMBER1);
    }

    private static Card card(long number){
        return new Card(number, TYPE, BALANCE, CREATED, CREATED);
    }

    public static Card updatedCard(Card card, int newBalance, Date lastUpdated){
        return new Card(card.number, card.type, newBalance, lastUpdated, card.created);
    }

    public static List<Card> cards(int number){
        List<Card> cards = new ArrayList<>();
        long curCardNumber = NUMBER1;
        for(int i = 0; i < number; i++){
            cards.add(card(curCardNumber));
            curCardNumber += 1;
        }
        return cards;
    }

    public static Payment payment() throws ParseException{
        return new Payment(DateUtil.fromString(DATE_TIME, "17:51:00 19.04.2018"),
                "Some check number",
                1000,
                "зачислено 1000 т.е.",
                "Some receiver",
                NUMBER1);
    }

    public static Payment laterPayment(Payment payment) throws ParseException{
        return new Payment(DateUtil.addDays(payment.getDateTime(), 1),
                "Some check number",
                1000,
                "зачислено 1000 т.е.",
                "Some receiver",
                NUMBER1);
    }

    public static MonthsStat stat() throws ParseException{
        return new MonthsStat(DateUtil.fromString(MONTH, "04.2018"),
                "some type",
                20,
                20 * 21,
                String.format("/card/transport/%016d/details", NUMBER1),
                NUMBER1,
                new Date(0));
    }

    public static MonthsStat laterStat(MonthsStat stat) throws ParseException{
        return new MonthsStat(addDays(stat.getPeriod(), 1),
                "some type",
                20,
                20 * 21,
                String.format("/card/transport/%016d/details", NUMBER1),
                NUMBER1,
                new Date(0));
    }

    public static MonthsStat statUpdated() throws ParseException{
        return new MonthsStat(DateUtil.fromString(MONTH, "04.2018"),
                "some type",
                22,
                22 * 21,
                String.format("/card/transport/%016d/details", NUMBER1),
                NUMBER1,
                new Date(0));
    }

    public static Ride ride() throws ParseException{
        return new Ride(DateUtil.fromString(DATE_TIME, "16:52:00 20.04.2018"),
                "Эл. кошелек",
                "63",
                21,
                DateUtil.fromString(MONTH, "04.2018"),
                NUMBER1);
    }

    public static Ride laterRide(Ride ride) throws ParseException{
        return new Ride(DateUtil.addDays(ride.getDateTime(), 1),
                "Эл. кошелек",
                "63",
                21,
                DateUtil.fromString(MONTH, "04.2018"),
                NUMBER1);
    }

    public static Ride rideWithEmptyRoute() throws ParseException{
        return new Ride(DateUtil.fromString(DATE_TIME, "16:52:00 20.04.2018"),
                "Эл. кошелек",
                "",
                21,
                DateUtil.fromString(MONTH, "04.2018"),
                NUMBER1);
    }
}
